function mok2txt(varargin)
%select input folder
if nargin == 0
    myTitle = 'LeXtender MOK2TXT';
    input_folder = uigetdir('', 'Input Folder');
    if input_folder == 0
        msgbox('No folder selected.', myTitle);
        clear input_folder
        close(mok2txt);
    end
end

%check for input files
data_files = dir(strcat(input_folder, '\*.mok3'));
if size(data_files) == 0
    msgbox('No NanoMOKE3 files found in input folder.', myTitle);
    close(mok2txt);
end

%select output folder
if nargin == 0
    output_folder = uigetdir('', 'Output Folder');
    if output_folder == 0
        msgbox('No folder selected.', myTitle);
        clear output_folder
        close(mok2txt);
    end
end

%generate automoke
output = automoke_header;
for i=1:size(data_files)
    importString = automoke_importloop(input_folder, data_files(i).name);
    exportString = automoke_export(output_folder, data_files(i).name);
    output = char(output, importString, exportString);
end
write_automoke('LeXtender_mok2txt.txt', output);

%wait for execution
while check_automoke('LeXtender_mok2txt.txt') == 0
    pause(1);
end
msgbox('Conversion successful.', myTitle);

%------------------------------------------------------------------------
%reusable functions
%------------------------------------------------------------------------
%generate AutoMOKE commands
%------------------------------------------------------------------------
function returnString = automoke_export(data_folder, filename)
line1 = strcat('export_loop_text_NI(', data_folder, '\', filename, '.txt)');
returnString = char(line1);

function returnString = automoke_importloop(data_folder, filename)
line1 = strcat('grab_open_NI(', data_folder, '\', filename);
returnString = char(line1);

function returnString = automoke_header
line1 = strcat('set_process()');
returnString = char(line1);

%------------------------------------------------------------------------
%import data
%------------------------------------------------------------------------
function finished = check_automoke(filename)
automoke_file = strcat('C:\NanoMOKEInbox\', filename);
if exist(automoke_file, 'file') == 0
    finished = 1;
else
    finished = 0;
end

%------------------------------------------------------------------------
%export data
%------------------------------------------------------------------------
function write_automoke(filename, output)
%automoke_file = strcat('C:\', filename)
automoke_file = filename;
inbox_file = strcat('C:\NanoMOKEInbox\', filename);
fileID = fopen(automoke_file, 'wt');
for i=1:size(output,1)
   fprintf(fileID, '%s\n', output(i,:));
end
fclose(fileID);
movefile(automoke_file, inbox_file)