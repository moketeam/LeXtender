function forc_import
dataFolder = uigetdir;
fileList = dir([dataFolder '\*.dat']);
numFiles = size(fileList, 1);
if numFiles > 0
    fieldsHa = NaN(numFiles,1);
    for i=1:numFiles
        fieldsHa(i) = str2num(strrep(sscanf(fileList(i).name, 'forc_data_%s') , '.txt', ''));
    end
    fieldsHa = sort(fieldsHa);
    forc = struct;
    for i=1:numFiles
        forc(i).fieldHa = fieldsHa(i);
        filename = strcat('forc_data_', num2str(fieldsHa(i)));
        forc(i).rawData = import_hysteresis(dataFolder, filename);
        forc(i).data = forc_extract(forc(i).rawData.data);
    end
    forc_config = struct;
    forc_config.data_folder = dataFolder;
    forc_config.sample = inputdlg('Enter sample name:', 'LeXtender FORC Recovery', 1, {'recovered FORC'});
    forc_config.fieldsHaRes = 0;
    forc_config.fieldsHbRes = 0;
    forc_config.fieldsHx = 0;
    forc_config.fieldsHxOffset = 0;
    forc_config.frequency = 0;
    forc_config.grab_max = 0;
    uisave({'forc', 'forc_config'}, 'recover.forc')
else
    msgbox('No FORC data files found in folder.', 'LeXtender FORC Recovery')
end

function hysteresis = forc_extract(data)
%positive und negative S�ttigung fitten
sat_neg = polyfit(data(106:140,3), data(106:140,5), 1);
sat_pos = polyfit(data(206:240,3), data(206:240,5), 1);
slope = mean([sat_neg(1); sat_pos(1)]);
offset = mean([sat_neg(2); sat_pos(2)]);
numHb = size(data,1);% + 400;
for i=401:numHb
    hx = data(i,3);
    kerr = data(i,5);
    corr_kerr = kerr - offset - slope * hx;
    j = i - 400;
    hyst(j,:) = [hx, corr_kerr];
end
hysteresis = hyst;

function importArray = import_hysteresis(data_folder, filename)
hyst_file = strcat(data_folder, '\', filename, '.txt');
delimiter = '\t';
header = 1;
importArray = importdata(hyst_file, delimiter, header);