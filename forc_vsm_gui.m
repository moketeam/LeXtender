function varargout = forc_vsm_gui(varargin)
% forc_vsm_gui MATLAB code for forc_vsm_gui.fig
%      forc_vsm_gui, by itself, creates a new forc_vsm_gui or raises the existing
%      singleton*.
%
%      H = forc_vsm_gui returns the handle to a new forc_vsm_gui or the handle to
%      the existing singleton*.
%
%      forc_vsm_gui('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in forc_vsm_gui.M with the given input arguments.
%
%      forc_vsm_gui('Property','Value',...) creates a new forc_vsm_gui or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before forc_vsm_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to forc_vsm_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help forc_vsm_gui

% Last Modified by GUIDE v2.5 22-Jun-2017 17:19:35
try
    % Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @forc_vsm_gui_OpeningFcn, ...
                       'gui_OutputFcn',  @forc_vsm_gui_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    % End initialization code - DO NOT EDIT
catch errGUI
    disp(errGUI)
end


% --- Executes just before forc_vsm_gui is made visible.
function forc_vsm_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to forc_vsm_gui (see VARARGIN)

%initialise processing cache
handles.proc = struct;

% Choose default command line output for forc_vsm_gui
handles.output = struct;
handles.output.control = 1;

% Update handles structure
guidata(hObject, handles);



% UIWAIT makes forc_vsm_gui wait for user response (see UIRESUME)
if nargin > 3
    uiwait(handles.figure1);
else
    handles.output.control = -1;
    guidata(hObject, handles);
    
    %Set default Unit to Tesla
    set(handles.uiUnits_Tesla,'Value',1);
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
if isequal(get(hObject, 'waitstatus'), 'waiting')
    % The GUI is still in UIWAIT, us UIRESUME
    uiresume(hObject);
else
    % The GUI is no longer waiting, just close it
    delete(hObject);
end


% --- Outputs from this function are returned to the command line.
function varargout = forc_vsm_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% The figure can be deleted now
if handles.output.control ~= -1
    delete(handles.figure1);
end




% --------------------------------------------------------------------
% Menu Functions
% --------------------------------------------------------------------
% --------------------------------------------------------------------
function File_Open_Forc_Callback(hObject, eventdata, handles)
% hObject    handle to File_Open_Forc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%let user select data file
importFolder = 'D:\Master Thesis\Picard MPMS3\DATA';
importFolder = uigetdir(importFolder, 'Select VSM FORC Data Path');
fileList = dir(fullfile(importFolder, '*.dat'));
numForcs = size(fileList, 1);

if numForcs == 0
    return
end

try

    
    
    %load data file
    endofheader = '[Data]';
    importData = struct([]);
    for i = 1:numForcs %vorher war i = 1:numHa-10 ??
        
        path = [importFolder '\' fileList(i).name];
        
        %Find end of Header in Data File
        file_id = fopen(path);
        line = 1;
        while strcmp(fgetl(file_id),endofheader)==0
            line = line+1;
        end
        
        importData(i).rawData = importdata(path,',',line+1);
        importData(i).filenames = fileList(i).name;
        importData(i).time = fileList(i).date;
    end

    %put forc in guidata
    handles.forc = struct;
    handles.forc = importData;
    handles.forc_config = struct;
    handles.forc_config.data_folder = importFolder;
    
  % Calculate recommended threshold
for i = 1:numForcs
    avg_error(i) = mean(handles.forc(i).rawData.data(:,6));
end
avg_error = mean(avg_error);
handles.recom_threshold = 4*avg_error;
recom_string = sprintf('Averaged Std. Error = %.4f \n \n => Recom. Threshold (4*avg. error )\n = %.4f', avg_error, handles.recom_threshold);
set(handles.uiRecom_threshold, 'String', recom_string);
set(handles.uiThreshold, 'String', num2str(handles.recom_threshold));
    

    %update guidata
    guidata(hObject, handles);
    ui_show_forc(handles);
    axes(handles.axesNoNoiseForc);
    cla;
catch errLoad
    disp(errLoad)
    return
end





% --------------------------------------------------------------------
% Plot selected Data
% --------------------------------------------------------------------

function ui_show_forc(handles)
%display Error
axes(handles.axesError)
cla
numForcs = size(handles.forc,2);
for i = 1:numForcs
    plot(handles.forc(i).rawData.data(:,6));
    hold on
end

recom_line = refline([0 handles.recom_threshold]);
recom_line.Color = 'black';
recom_line.LineWidth = 1;
curr_threshold = str2num(get(handles.uiThreshold, 'String'));
curr_line = refline([0 curr_threshold]);
curr_line.Color = 'r';
curr_line.LineWidth = 1;
hold off



%display NoiseForc
axes(handles.axesNoiseForc)
cla
for i = 1:numForcs
    plot(handles.forc(i).rawData.data(:,4),handles.forc(i).rawData.data(:,5));
    hold on
end
xlabel('Hb (Oe)')
ylabel('M (emu)')




% --- Executes on button press in uiOpenFigureError.
function uiOpenFigureError_Callback(hObject, eventdata, handles)
% hObject    handle to uiOpenFigureForc_with_noise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
figure('Name', 'Std. Error')
numForcs = size(handles.forc,2);
for i = 1:numForcs
    plot(handles.forc(i).rawData.data(:,6));
    hold on
end

recom_line = refline([0 handles.recom_threshold]);
recom_line.Color = 'black';
recom_line.LineWidth = 1;
curr_threshold = str2num(get(handles.uiThreshold, 'String'));
curr_line = refline([0 curr_threshold]);
curr_line.Color = 'r';
curr_line.LineWidth = 1;

hold off

   

% --- Executes on button press in uiOpenFigureForc_with_noise.
function uiOpenFigureForc_with_noise_Callback(hObject, eventdata, handles)
% hObject    handle to uiOpenFigureError (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

figure('Name', 'Forc raw data')
%display NoiseForc
numForcs = size(handles.forc,2);
for i = 1:numForcs
    plot(handles.forc(i).rawData.data(:,4),handles.forc(i).rawData.data(:,5));
    hold on
end
xlabel('Hb (Oe)')
ylabel('M (emu)')
hold off


% --- Executes on button press in uiOpenFigureForc_reduced_noise.
function uiOpenFigureForc_reduced_noise_Callback(hObject, eventdata, handles)
% hObject    handle to uiOpenFigureForc_reduced_noise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

figure('Name', 'Forc with reduced noise and demag corrected')

demagCheckbox = get(handles.uiCheckboxDemag,'Value');

if demagCheckbox == 1
demagFactor = str2num(get(handles.uiDemagFactor,'String'));
sampleVolume = str2num(get(handles.uiSampleVolume,'String'));
elseif demagCheckbox == 0
demagFactor = NaN;
sampleVolume = NaN;
end


%load processing configuration
threshold = str2num(get(handles.uiThreshold, 'String'));

%display no noise Forc
numForcs = size(handles.forc,2);
for i=1:numForcs
    
    no_noise = noise_redu(handles.forc(i).rawData.data,threshold);
    if demagCheckbox == 1
        no_noise(:,4) = (no_noise(:,4)-no_noise(:,5)*(1.0/sampleVolume)*demagFactor*4*pi)/10000.0;
    elseif demagCheckbox == 0
         no_noise(:,4) = (no_noise(:,4))/10000.0;
    end
    plot(no_noise(:,4),no_noise(:,5));
    hold on
end

xlabel('Hb (Oe)')
ylabel('M (emu)')
hold off





% --- Executes on button press in uiSave.
function uiSave_Callback(hObject, eventdata, handles)
% hObject    handle to uiSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Oe = get(handles.uiUnits_Oe,'Value');
Tesla = get(handles.uiUnits_Tesla,'Value');
demagCheckbox = get(handles.uiCheckboxDemag,'Value');

if demagCheckbox == 1
demagFactor = str2num(get(handles.uiDemagFactor,'String'));
sampleVolume = str2num(get(handles.uiSampleVolume,'String'));
elseif demagCheckbox == 0
demagFactor = NaN;
sampleVolume = NaN;
end


%Noise Reduce
numForcs = size(handles.forc,2);
threshold = str2num(get(handles.uiThreshold, 'String'));
for i=1:numForcs
    handles.forc(i).rawData.data = noise_redu(handles.forc(i).rawData.data,threshold);
end


numForcs = size(handles.forc,2);
for i=1:numForcs
    
    maxMoment(i) = max(handles.forc(i).rawData.data(:,5));
    %Read magnetic field from data file with or without demag. factor
    %divided by 10000 to convert Oe to T
    if demagCheckbox == 1
        handles.forc(i).data(:,1) = (handles.forc(i).rawData.data(:,4)...
            -handles.forc(i).rawData.data(:,5)*(1.0/sampleVolume)*demagFactor*4*pi)/10000.0;
    elseif demagCheckbox == 0
        handles.forc(i).data(:,1) = (handles.forc(i).rawData.data(:,4))/10000.0;
    end
    %Read magnetic moment from data file
    handles.forc(i).data(:,2) = handles.forc(i).rawData.data(:,5);
    
    Hmin(i) = min(handles.forc(i).data(:,1));
    Hmax(i) = max(handles.forc(i).data(:,1));
    Ha(i) = Hmin(i);
end

H_Max = max(Hmax);
Ha = Ha/H_Max;
Max_Moment = max(maxMoment);

for i = 1:numForcs
    handles.forc(i).fieldHa = Ha(i);
    handles.forc(i).data(:,2) = handles.forc(i).data(:,2)/Max_Moment;
end

 sortHa = sort(Ha);
    forc = struct([]);
    for i=1:numForcs
        j = find(Ha==sortHa(i), 1, 'first');
        if i==1
            forc = handles.forc(j);
        else
            forc(i) = handles.forc(j);
        end
    end

handles.forc = del_double_Hrev(handles.forc,H_Max);
numForcs = size(handles.forc,2);

    
handles.forc_config.sample = inputdlg('Sample Name:', 'VSM FORC Sample Name');
handles.forc_config.fieldsHaRes = numForcs;
handles.forc_config.fieldsHx = H_Max;
handles.forc_config.noise_redu_threshold = str2num(get(handles.uiThreshold, 'String'));
handles.forc_config.demagFactor = demagFactor;
handles.forc_config.sampleVolume = sampleVolume; 

if Oe == 1
    handles.forc_config.unit = 'Oe';
end
if Tesla == 1
    handles.forc_config.unit = 'Tesla';
end


[saveFile,savePath] = uiputfile({'*.forc', 'FORC Data File'}, 'Save FORC Data File');
handles.forc_config.acq_file = fullfile(savePath,saveFile);

forc_config = handles.forc_config;
forc = handles.forc;

save(handles.forc_config.acq_file, 'forc', 'forc_config')




% --- Executes on button press in uiShow.
function uiShow_Callback(hObject, eventdata, handles)
% hObject    handle to uiShow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
demagCheckbox = get(handles.uiCheckboxDemag,'Value');

if demagCheckbox == 1
demagFactor = str2num(get(handles.uiDemagFactor,'String'));
sampleVolume = str2num(get(handles.uiSampleVolume,'String'));
elseif demagCheckbox == 0
demagFactor = NaN;
sampleVolume = NaN;
end


%load processing configuration
threshold = str2num(get(handles.uiThreshold, 'String'));

%display no noise Forc
axes(handles.axesNoNoiseForc)
cla
numForcs = size(handles.forc,2);
for i=1:numForcs
    
    no_noise = noise_redu(handles.forc(i).rawData.data,threshold);
    if demagCheckbox == 1
        no_noise(:,4) = (no_noise(:,4)-no_noise(:,5)*(1.0/sampleVolume)*demagFactor*4*pi)/10000.0;
    elseif demagCheckbox == 0
         no_noise(:,4) = (no_noise(:,4))/10000.0;
    end
    plot(no_noise(:,4),no_noise(:,5));
    hold on
end

% for i=1:numForcs
%     no_noise = noise_redu(handles.forc(i).rawData.data,threshold);
%     plot(no_noise(:,4),no_noise(:,5));
%     hold on
% end
xlabel('Hb (Oe)')
ylabel('M (emu)')
ui_show_forc(handles)




function uiThreshold_Callback(hObject, eventdata, handles)
% hObject    handle to uiThreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of uiThreshold as text
%        str2double(get(hObject,'String')) returns contents of uiThreshold as a double
ui_show_forc(handles);


% --- Executes during object creation, after setting all properties.
function uiThreshold_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uiThreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





function uiRecom_threshold_Callback(hObject, eventdata, handles)
% hObject    handle to uiRecom_threshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of uiRecom_threshold as text
%        str2double(get(hObject,'String')) returns contents of uiRecom_threshold as a double


% --- Executes during object creation, after setting all properties.
function uiRecom_threshold_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uiRecom_threshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in uiUnits_Tesla.
function uiUnits_Tesla_Callback(hObject, eventdata, handles)
% hObject    handle to uiUnits_Tesla (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of uiUnits_Tesla

Oe = get(handles.uiUnits_Oe,'Value');
if Oe == 1
    set(handles.uiUnits_Oe,'Value',0);
end



% --- Executes on button press in uiUnits_Oe.
function uiUnits_Oe_Callback(hObject, eventdata, handles)
% hObject    handle to uiUnits_Oe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of uiUnits_Oe

Tesla = get(handles.uiUnits_Tesla,'Value');
if Tesla == 1
    set(handles.uiUnits_Tesla,'Value',0);
end



% Function for deleting FORC's with the same reversal Field
function forc = del_double_Hrev(forc, fieldsHx)

fieldsHa = NaN(size(forc,2),1);
for i=1:size(forc,2)
    fieldsHa(i) = forc(i).fieldHa * fieldsHx*10000.0;
end
fieldsHa = sort(fieldsHa);

    %create array with all occurring H_rev field steps
hx_diff = NaN(size(forc,2),1);
for i=1:(size(fieldsHa,1)-1)
    hx_diff(i) = abs(fieldsHa(i+1) - fieldsHa(i));   
end

    %find steps which are too small compared to the neighbouring
delete = [];
for i=2:(size(hx_diff,1)-1)
    
    if 5*hx_diff(i) < hx_diff(i-1) && 5*hx_diff(i) < hx_diff(i+1)
        delete = [delete i];
    end
end

forc(delete) = [];


% --- Executes on button press in uiCheckboxDemag.
function uiCheckboxDemag_Callback(hObject, eventdata, handles)
% hObject    handle to uiCheckboxDemag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of uiCheckboxDemag


if get(handles.uiCheckboxDemag,'Value') == 1;
    set(handles.textDemag, 'Enable', 'on');
    set(handles.textVolume, 'Enable', 'on');
    set(handles.uiDemagFactor, 'Enable', 'on');
    set(handles.uiSampleVolume, 'Enable', 'on');
end

if get(handles.uiCheckboxDemag,'Value') == 0;
    set(handles.textDemag, 'Enable', 'off');
    set(handles.textVolume, 'Enable', 'off');
    set(handles.uiDemagFactor, 'Enable', 'off');
    set(handles.uiSampleVolume, 'Enable', 'off');
end

% --- Executes during object creation, after setting all properties.
function uiCheckboxDemag_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uiThreshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




