function nForc = forc_cut(forcData, Ha, Hb)
%initialise data structure
nForc = struct;
nForc.data = struct;
nForc.config = forcData.config;
nForc.config.fieldsHx = Ha;
forc = forcData.data;
numForc = size(forc, 2);
newSize = 1;

%cut off data
for i=1:numForc
    if abs(min(forc(i).data(:,1))) < Ha
        startIndex = find(forc(i).data(:,1) == min(forc(i).data(:,1)));
        endIndex = find(forc(i).data(:,1) > Hb, 1, 'first');
        newData = forc(i).data(startIndex:endIndex,:);
        if size(newData,1) > 1
            nForc.data(newSize).data = newData;
            nForc.data(newSize).fieldHa = nForc.data(newSize).data(1,1) / nForc.config.fieldsHx;
            nForc.data(newSize).rawData = forc(i).rawData;
            nForc.data(newSize).filenames = forc(i).filenames;
            nForc.data(newSize).time = forc(i).time;
            newSize = newSize + 1;
        end
    end
end
numNForc = newSize - 1;

%find new Hb length
lengthHb = size(nForc.data(1).data,1);
for i=1:numNForc
    if size(nForc.data(i).data,1) > lengthHb
        lengthHb = size(nForc.data(i).data,1);
    end
end

%interpolate
for i=1:numNForc
    
    minHb = min(nForc.data(i).data(:,1));
    maxHb = max(nForc.data(i).data(:,1));
    stepHb = (maxHb - minHb) / (lengthHb-1);
    newHb = minHb:stepHb:maxHb;
    

    try
        newKerr = interp1(nForc.data(i).data(:,1), nForc.data(i).data(:,2), newHb);
    catch errDuplicateHb
        %do stuff when interpolation fails due to double values of Hb
        for j=1:size(nForc.data(i).data(:,1))
            for k=j+1:size(nForc.data(i).data(:,1))
                if nForc.data(i).data(j,1) == nForc.data(i).data(k,1)
                    %add 10 �Oe if two Hb values overlap
                    nForc.data(i).data(k,1) = nForc.data(i).data(k,1) + 0.00001;
                end
            end
        end
        newKerr = interp1(nForc.data(i).data(:,1), nForc.data(i).data(:,2), newHb);
    end


    newKerr = interp1(nForc.data(i).data(:,1), nForc.data(i).data(:,2), newHb);
    nForc.data(i).data = NaN(lengthHb,2);
    nForc.data(i).data(:,1) = newHb;
    nForc.data(i).data(:,2) = newKerr;
end

nForc.config.fieldsHbRes = size(nForc.data(1).data, 1);