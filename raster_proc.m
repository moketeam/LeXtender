function result = raster_proc
%open acquisition file
[data_file, data_path] = open_dialog;
wb_title = 'LeXtender Raster Mapping Processing';
%check data_file
if exist('data_file', 'var') == 0
    return
end
[config, dataPoints] = import_acq([data_path data_file]);

%initialise data storage
num_dataPoints = size(dataPoints,1);
num_fields = 1000;
field = struct;
field.Hx = zeros(num_fields,1);

img_size = sqrt(size(dataPoints,1));
%img_posX = zeros(img_size,1);
%img_posY = zeros(img_size,1);
procData = zeros(img_size, img_size, num_fields);
maps = struct;
maps.Hc = zeros(img_size, img_size);
maps.Eb = zeros(img_size, img_size);
maps.Amp = zeros(img_size, img_size);
maps.Rem = zeros(img_size, img_size);

aveData = struct;
aveData.procHyst = zeros(num_fields, 1);
aveData.Hc = 0;
aveData.Eb = 0;
aveData.Amp = 0;
aveData.Rem = 0;

import = struct;
%import.hyst = struct;
%import.proc = struct;

%import files
wb = cwaitbar(0, 'Processing data ...', 'Name', wb_title);
for i=1:num_dataPoints
    cwaitbar(i/num_dataPoints, wb)
    %import data
    curFile = [data_path 'y' num2str(dataPoints(i,1)) '_x' num2str(dataPoints(i,2))];
    import.proc = import_proc(curFile);
    import.hyst = import_hyst(curFile);
    %non images
    field.Hx = field.Hx + import.hyst(:,1);
    aveData.procHyst = aveData.procHyst + import.hyst(:,2);
    aveData.Hc = aveData.Hc + import.proc.Hc;
    aveData.Eb = aveData.Eb + import.proc.Eb;
    aveData.Amp = aveData.Amp + import.proc.Amp;
    aveData.Rem = aveData.Rem + import.proc.Rem;
    %images
    img_posX = int8((dataPoints(i,2) + config.Position + config.Step) / config.Step);
    img_posY = int8(1 + img_size - ((dataPoints(i,1)  + config.Position + config.Step) / config.Step));
    %data stack
    procData(img_posY, img_posX, :) = reshape(import.hyst(:,2), 1, 1, []);
    maps.Hc(img_posY, img_posX) = import.proc.Hc;
    maps.Eb(img_posY, img_posX) = import.proc.Eb;
    maps.Amp(img_posY, img_posX) = import.proc.Amp;
    maps.Rem(img_posY, img_posX) = import.proc.Rem;
end
delete(wb)

%normalising averages
field.Hx = field.Hx / num_dataPoints;
aveData.procHyst = aveData.procHyst / num_dataPoints;
aveData.Hc = aveData.Hc / num_dataPoints;
aveData.Eb = aveData.Eb / num_dataPoints;
aveData.Amp = aveData.Amp / num_dataPoints;
aveData.Rem = aveData.Rem / num_dataPoints;

%prepare output
result = struct;
result.hyst = procData;
result.field = field;
result.average = aveData;
result.maps = maps;

%------------------------------------------------------------------------
%open file dialog
%------------------------------------------------------------------------
function [data_file, data_path] = open_dialog
[data_file, data_path] = uigetfile({'acquistion.txt;acquisition.txt','LeXtender Acquisition File'});
if data_file ~= 0
    if exist(data_file, 'file') ~= 0
        msgbox('No valid file selected.', 'LeXtender Processing')
        clear data_file
        clear data_path
    end
else
    msgbox('No file selected.', 'LeXtender Processing')
    clear data_file
    clear data_path
end

%------------------------------------------------------------------------
%import functions
%------------------------------------------------------------------------
function [config, dataPoints] = import_acq(dataFile)
dataImport = importdata(dataFile);
dataPoints = dataImport.data;
config = struct;
config.Position = str2num(strrep(deblank(dataImport.textdata{1,1}), 'Position=', ''));
config.Resolution = str2num(strrep(deblank(dataImport.textdata{2,1}), 'Resolution=', ''));
config.Step = 2 * config.Position / config.Resolution;
config.Field = str2num(strrep(deblank(dataImport.textdata{3,1}), 'Field=', ''));
config.Frequency = str2num(strrep(deblank(dataImport.textdata{4,1}), 'Frequency=', ''));
config.Averaging = str2num(strrep(deblank(dataImport.textdata{5,1}), 'Averaging=', ''));
config.Holding = str2num(strrep(deblank(dataImport.textdata{6,1}), 'Holding=', ''));

function hyst = import_hyst(dataFile)
hystFile = [dataFile '.txt'];
import = importdata(hystFile);
hyst = import.data;

function proc = import_proc(dataFile)
procFile = [dataFile '_proc.txt'];
import = importdata(procFile, '=');
proc = struct;
proc.Hc = import.data(1);
proc.Eb = import.data(2);
proc.Rem = import.data(3);
proc.Amp = import.data(4);