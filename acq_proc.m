%------------------------------------------------------------------------
%convert proc files to images
%------------------------------------------------------------------------
function acq_proc(varargin)
%check varargin
if nargin == 0
    [data_file, data_path] = open_dialog;
    wb_title = 'LeXtender Processing';
else
    data_file = varargin{1};
    data_path = varargin{2};
    wb_title = 'LeXtender Acquisition';
end

%check data_file
if exist('data_file', 'var') == 0
    return
end

%load acquisition header
acq_file = importdata(strcat(data_path, data_file), ',', 6);
points = acq_file.data;

%calculate acquisition parameters
num_points = size(points,1);
acq_pos = strrep(deblank(acq_file.textdata{1,1}), 'Position=', '');
acq_pos = str2double(acq_pos);
acq_res = strrep(deblank(acq_file.textdata{2,1}), 'Resolution=', '');
acq_res = str2double(acq_res);
acq_step = 2 * acq_pos / acq_res;
image_dimension = acq_res + 1;

%decide on parallel processing
if image_dimension > 99
    par_proc = 1;
    
    %start matlabpool
    wb = waitbar(0, 'Starting up parallelisation...', 'Name', wb_title);
    if matlabpool('size') == 0
        matlabpool
    end
    delete(wb);
else
    par_proc = 0;
end

%update waitbar
wb = cwaitbar(0, 'Loading data...', 'Name', wb_title);

%intialize data
data_dimension = acq_res + 2;
dataHc = zeros(data_dimension);
dataEb = zeros(data_dimension);
dataRem = zeros(data_dimension);
dataAmp = zeros(data_dimension);
badPoints = zeros(data_dimension);
num_badPoints = 0;
yPoints = points(:,1);
xPoints = points(:,2);

parfor i=1:num_points
    %update waitbar
    if par_proc == 0
        bar_pos = num_points - i;
        cwaitbar(bar_pos/num_points, wb);
    end
    
    %determine X,Y position
    currentY = yPoints(i);
    currentX = xPoints(i);
    currentFile = strcat('y', num2str(currentY), '_x', num2str(currentX));
    
    %load data
    currentArray = import_processed(data_path, currentFile); 
    dataHc(i) = currentArray.data(1,1);
    dataEb(i) = currentArray.data(2,1);
    dataRem(i) = currentArray.data(3,1);
    dataAmp(i) = currentArray.data(4,1);
    
    %check data
    %no negative values
    if (dataHc(i) < 0) || (dataRem(i) < 0) || (dataAmp(i) < 0)
        dataHc(i) = 0;
        dataEb(i) = 0;
        dataRem(i) = 0;
        dataAmp(i) = 0;
        badPoints(i) = 1;
    end
end

%update waitbar
wb = cwaitbar(0, wb, 'Sorting data...', 'Name', wb_title);

%intialize image arrays
imageHc = zeros(image_dimension, image_dimension);
imageEb = zeros(image_dimension, image_dimension);
imageRem = zeros(image_dimension, image_dimension);
imageAmp = zeros(image_dimension, image_dimension);
badImage = zeros(image_dimension, image_dimension);

%sort data
for i=1:num_points
    %update waitbar
    cwaitbar(i/num_points, wb);
    
    %determine X,Y position and load data
    currentY = points(i,1);
    currentX = points(i,2);
    
    %determine pixel position and insert data
    imX = int8((currentX + acq_pos + acq_step) / acq_step);
    imY = int8(image_dimension - ((currentY + acq_pos + acq_step) / acq_step) + 1);
    if (imY ~= 0) && (imX ~= 0)
        imageHc(imY,imX) = dataHc(i);
        imageEb(imY,imX) = dataEb(i);
        imageRem(imY,imX) = dataRem(i);
        imageAmp(imY,imX) = dataAmp(i);
        badImage(imY,imX) = badPoints(i);
        if badPoints(i) == 1
            num_badPoints = num_badPoints + 1;
        end
    end
end

%kill waitbar
delete(wb);

%import photo and show images
imageRef = importdata(strcat(data_path, 'photo.txt'));
%use imagesc (distributable)
figure('Name','Reflectivity','NumberTitle','off')
imagesc(imageRef)
figure('Name','Coercivity','NumberTitle','off')
imagesc(imageHc)
figure('Name','Exchange Bias','NumberTitle','off')
imagesc(imageEb)
figure('Name','Remanence','NumberTitle','off')
imagesc(imageRem)
figure('Name','Kerr Amplitude','NumberTitle','off')
imagesc(imageAmp)

%bad pixel warning
if num_badPoints ~= 0
    figure('Name','Bad Pixels','NumberTitle','off')
    imagesc(badImage)
    msgbox(strcat(num2str(num_badPoints), ' bad pixels'), wb_title)
end

%stop matlabpool
if par_proc == 1
    wb = waitbar(0, 'Stopping parallelisation...', 'Name', wb_title);
    %matlabpool close
    delete(wb);
end

%------------------------------------------------------------------------
%import data
%------------------------------------------------------------------------
function importArray = import_processed(data_folder, filename)
proc_file = strcat(data_folder, filename, '_proc.txt');
delimiter = '=';
tempArray = importdata(proc_file, delimiter);
for i=1:4
    tempArray.textdata(i,1) = strtrim(tempArray.textdata(i,1));
end
if size(tempArray.data) < 4
    tempArray.data(1,1) = -1;
    tempArray.data(2,1) = 0;
    tempArray.data(3,1) = -1;
    tempArray.data(4,1) = -1;
end
importArray = tempArray;


%------------------------------------------------------------------------
%open file dialog
%------------------------------------------------------------------------
function [data_file, data_path] = open_dialog
[data_file, data_path] = uigetfile({'acquistion.txt;acquisition.txt','LeXtender Acquisition File'});
if data_file ~= 0
    if exist(data_file, 'file') ~= 0
        msgbox('No valid file selected.', 'LeXtender Processing')
        clear data_file
        clear data_path
    end
else
    msgbox('No file selected.', 'LeXtender Processing')
    clear data_file
    clear data_path
end