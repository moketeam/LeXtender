try
    importFolder = 'D:\Master Thesis\Picard MPMS3\DATA';
    importFolder = uigetdir(importFolder, 'Select VSM FORC Data Path');
    fileList = dir(fullfile(importFolder, '*.dat'));
    
    numHa = size(fileList, 1);
    Hmin = NaN(numHa, 1);
    Hmax = NaN(numHa, 1);
    Ha = NaN(numHa, 1);
    importData = struct([]);
    smoothingRange = str2double(inputdlg('Smoothing Range:', 'VSM FORC Smoothing'));
%% Start
    er = figure;
    for i = 1:numHa %vorher war i = 1:numHa-10 ??
        path = [importFolder '\' fileList(i).name];
        forc = csvread(path,28,1);
        er = plot(forc(:,5));
        hold on
    end
    waitfor(er);
    th = str2double(inputdlg('Threshold:', 'VSM FORC noise threshold'));
    for i=1:numHa
        importFile = fullfile(importFolder, fileList(i).name);
        importCSV = csvread(importFile, 28, 1);
        importCSV = noise_redu(importCSV,th);
        maxMoment(i) = max(importCSV(:,4));
        
        importData(i).forc.data(:,2) = importCSV(:,4);
%        importData(i).forc.data(:,1) = importCSV(:,3);
        importData(i).forc.data(:,1) = (importCSV(:,3)-importCSV(:,4)*(1.0/0.002)*0.2)/10000.0;
    %attention attention: data is provided in T instead of Oe!!!!!!
%% End 
        importData(i).forc.filenames.data = fileList(i).name;
        importData(i).forc.time = fileList(i).date;
        Hmin(i) = min(importData(i).forc.data(:,1));
        Hmax(i) = max(importData(i).forc.data(:,1));
        Ha(i) = Hmin(i);%/Hmax(i);
        %importData(i).forc.fieldHa = Ha(i);
    end
    H_Max = max(Hmax);
    Ha = Ha / H_Max;
    Max_Moment = max(maxMoment);
    for i = 1:numHa
        importData(i).forc.fieldHa = Ha(i);
        importData(i).forc.data(:,2) = importData(i).forc.data(:,2)/Max_Moment;
    end
    
    
    sortHa = sort(Ha);
    forc = struct([]);
    for i=1:numHa
        j = find(Ha==sortHa(i), 1, 'first');
        if i==1
            forc = importData(j).forc;
        else
            forc(i) = importData(j).forc;
        end
    end
    
    forc_config = struct;
    forc_config.data_folder = importFolder;
    forc_config.sample = inputdlg('Sample Name:', 'VSM FORC Sample Name');
    forc_config.fieldsHaRes = numHa;
    forc_config.fieldsHx = H_Max;
    [saveFile,savePath] = uiputfile({'*.forc', 'FORC Data File'}, 'Save FORC Data File');
    forc_config.acq_file = fullfile(savePath,saveFile);
    
    save(forc_config.acq_file, 'forc', 'forc_config')
    
catch errImport
    msgbox(errImport.message, errImport.identifier)
end