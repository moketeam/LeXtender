function output = sweep_proc(handles)
sweep = handles.sweep.data;
ticAll = tic;
wb_title = 'LeXtender Sweep Mapping Processing';

%initialise data storage
num_fields = size(sweep,2);

bin = get(handles.checkboxBinPixels, 'Value');

if bin == 1
    binFactor = str2double(get(handles.editBin, 'String'));
else
    binFactor = 1;
end

img_size = 512 / binFactor;

field = struct;
field.Hx = zeros(num_fields,1);
field.pos = zeros(num_fields/2,1);
field.neg = zeros(num_fields/2,1);
field.direction = zeros(num_fields,1);

rawData = zeros(img_size, img_size, num_fields);
procData = zeros(img_size, img_size, num_fields);

aveData = struct;
aveData.rawHyst = zeros(num_fields,1);
aveData.procHyst = zeros(num_fields,1);
aveData.Hb = 0;
aveData.Eb = 0;
aveData.Amp = 0;
aveData.Rem = 0;

maps = struct;
maps.Hc = zeros(img_size,img_size);
maps.Eb = zeros(img_size,img_size);
maps.Amp = zeros(img_size,img_size);
maps.Rem = zeros(img_size,img_size);

%sort sweeps into hystereses
wb = cwaitbar(0, 'Creating hystereses from sweeps...', 'Name', wb_title);
for m = 1:num_fields
    cwaitbar(m/num_fields, wb)
    %bin image
    swpBin = imresize(sweep(m).rawData, 1/binFactor, 'box');
    %create average hystereses
    aveData.rawHyst(m) = mean(mean(swpBin));
    field.Hx(m) = sweep(m).fieldHx;
    field.direction(m) = sweep(m).direction;
    %create individual hystereses
    rawData(:,:,m) = swpBin;
end
field.pos = field.Hx(field.direction == 1);
field.neg = field.Hx(field.direction == -1);

%process hystereses
wb = cwaitbar(0, wb, 'Processing hystereses...');
temp = proc_rawHyst(handles, aveData.rawHyst, field);
aveData.procHyst = temp.hyst;
aveData.Hc = temp.Hc;
aveData.Eb = temp.Eb;
aveData.Amp = temp.Amp;
aveData.Rem = temp.Rem;
loopTic = tic;
for i=1:img_size
    loopTime = toc(loopTic);
    remainTime = round(loopTime/i * (img_size - i));
    cwaitbar(i/img_size, wb, ['Processing hystereses (' num2str(remainTime) ' seconds remaining) ...'])
    sliceHc = zeros(1,img_size);
    sliceEb = zeros(1,img_size);
    sliceAmp = zeros(1,img_size);
    sliceRem = zeros(1,img_size);
    for j=1:img_size
        %warning('off','all');
        %try
            temp = proc_rawHyst(handles, reshape(rawData(i,j,:),[],1), field);
            sliceHc(1,j) = temp.Hc;
            sliceEb(1,j) = temp.Eb;
            sliceAmp(1,j) = temp.Amp;
            sliceRem(1,j) = temp.Rem;
            procData(i,j,:) = temp.hyst;
        %catch
        %    disp(['i=', num2str(i), ', j=', num2str(j)])
        %end
    end
    maps.Hc(i,:) = sliceHc;
    maps.Eb(i,:) = sliceEb;
    maps.Amp(i,:) = sliceAmp;
    maps.Rem(i,:) = sliceRem;
end
delete(wb)

%output
output = struct;
output.average = aveData;
output.field = field;
output.maps = maps;
output.hyst = procData;
output.raw = rawData;
disp(['Elapsed Time: ' num2str(toc(ticAll)) ' Seconds'])


function proc_result = proc_rawHyst(handles, rawData, field)
%initialise return variable
proc_result = struct;

spikeCorrection = get(handles.checkboxSpikeFilter, 'Value');
spikeRecursive = get(handles.checkboxRecursive, 'Value');
spikeFactor = str2double(get(handles.editSpike, 'String'));
ellipsCorrection = get(handles.checkboxEllipse, 'Value');

if spikeCorrection == 1
    %remove spikes
    noSpikeHyst = proc_spikes(rawData, spikeRecursive, spikeFactor);
else
    noSpikeHyst = rawData;
end

%remove temporal drift
driftHyst = proc_drift(noSpikeHyst);

%split rising and falling field parts
posHyst = driftHyst(field.direction == 1);
negHyst =  driftHyst(field.direction == -1);

if ellipsCorrection == 1
    proc_pos.ellip = proc_halfHyst_ellipse(posHyst, field.pos);
    proc_neg.ellip = proc_halfHyst_ellipse(negHyst, field.neg);
    proc_result.ellip = (proc_pos.ellip - proc_neg.ellip) / 2;
    posHyst = posHyst - 0.5*proc_result.ellip;
    negHyst = negHyst + 0.5*proc_result.ellip;
end


%process half hystereses and determine slope, offset
proc_pos.slope = proc_halfHyst_slope(posHyst, field.pos);
proc_neg.slope = proc_halfHyst_slope(negHyst, field.neg);
proc_result.slope = (proc_pos.slope + proc_neg.slope) / 2;
posHyst = posHyst - proc_result.slope * field.pos;
negHyst = negHyst - proc_result.slope * field.neg;

proc_pos.offset = proc_halfHyst_offset(posHyst);
proc_neg.offset = proc_halfHyst_offset(negHyst);
proc_result.offset = (proc_pos.offset + proc_neg.offset) / 2;
posHyst = posHyst - proc_result.offset;
negHyst = negHyst - proc_result.offset;

%determine amplitude
proc_result.Amp = (calc_amplitude(posHyst) + calc_amplitude(negHyst)) / 2;

%find zero crossing
zeroCross_pos = calc_zeroCross(posHyst, field.pos);
zeroCross_neg = calc_zeroCross(negHyst, field.neg);
if ~(isnan(zeroCross_pos) || isnan(zeroCross_neg))
    %only go on if clear zero crossing exists
    %determine Hc and EB
    try
        proc_result.Hc = (zeroCross_pos - zeroCross_neg) / 2;
        proc_result.Eb = (zeroCross_pos + zeroCross_neg) / 2;
    catch errZeroCross
        disp(errZeroCross)
    end
    %determine remanence
    pos_rem = abs(proc_remanence(posHyst, field.pos));
    neg_rem = abs(proc_remanence(negHyst, field.neg));
    proc_result.Rem = (pos_rem + neg_rem) / proc_result.Amp;
else
    proc_result.Hc = NaN;
    proc_result.Eb = NaN;
    proc_result.Rem = NaN;
end

%combine positive and negative half hysteresis
proc_result.hyst = cat(1, posHyst, negHyst);


function proc_result = proc_spikes(rawHyst, spikeRecursive, spikeFactor)
compHyst = rawHyst;

%get rid of spikes
diffRawHyst = diff(rawHyst);
spikes = (find(abs(diffRawHyst)>spikeFactor*std(diffRawHyst)))';

if ~isempty(spikes)
    %This can filter spikes with up to 3 consequtive datapoint off curve
    diffSpikes = diff(spikes);
    for i=1:length(diffSpikes)
        %up to 3 data points off curve which is a up and down spike so
        %magnetic switching does not get filtered when next to a spike
        if diffSpikes(i) <= 3 && sign(diffRawHyst(spikes(i))) ~= sign(diffRawHyst(spikes(i+1)))
            %take care of boundaries first
            if spikes(i) == 1
                newHystVal = rawHyst(spikes(i+1)+1);
            elseif spikes(i) == length(diffRawHyst)
                newHystVal = rawHyst(spikes(i)-1);
            else
                %calculate new values for off data points from first and last
                %point off real data
                newHystVal = mean([rawHyst(spikes(i)-1) rawHyst(spikes(i+1)+1)]);
            end
            %set all points which belong to one spike to the new value
            for j=spikes(i):(spikes(i)+diffSpikes(i))
                rawHyst(j) = newHystVal;
            end
        end
    end
end

if ~isequal(compHyst, rawHyst) && spikeRecursive == 1
    rawHyst = proc_spikes(rawHyst, spikeRecursive, spikeFactor);
end
    
proc_result = rawHyst;


function proc_result = proc_drift(noSpikeHyst)
%hysteresis size
xTime = reshape(1:size(noSpikeHyst, 1),[],1);

%calcualte/substract slope
fitxTime = cat(1,xTime(1:round(length(xTime)/10)), xTime(end-round(length(xTime)/10):end));
fitHyst = cat(1,noSpikeHyst(1:round(length(noSpikeHyst)/10)), noSpikeHyst(end-round(length(noSpikeHyst)/10):end));
fit = polyfit(fitxTime, fitHyst, 1);
slope = fit(1);

%substract slope
%Amazing FGross (tm) (c) (R) buggettho^9238 fixxxxxxxxxx
noSpikeHyst = noSpikeHyst - (slope-1e-10)*xTime;

%calcualte/substract offset
offset = mean(noSpikeHyst);
noSpikeHyst = noSpikeHyst - offset;

%return data
proc_result = noSpikeHyst;


function proc_result = proc_halfHyst_ellipse(halfHyst, field)

fit = polyfit(field,halfHyst,2);

proc_result = polyval(fit,field);


function proc_result = proc_halfHyst_slope(halfHyst, field)
%hysteresis size
hystSize = size(halfHyst, 1);

%fit negative saturation
negMax = round(hystSize / 5);
negFit = polyfit(field(1:negMax), halfHyst(1:negMax), 1);

%fit positive saturation
posMin = hystSize - round(hystSize / 5);
posFit = polyfit(field(posMin:end), halfHyst(posMin:end), 1);

%determine slope
proc_result = (negFit(1) + posFit(1)) / 2;


function proc_result = proc_halfHyst_offset(halfHyst)
%hysteresis size
hystSize = size(halfHyst, 1);

%get negative saturation
negMax = round(hystSize / 5);
negOffset = mean(halfHyst(1:negMax));

%get positive saturation
posMin = hystSize - round(hystSize / 5);
posOffset = mean(halfHyst(posMin:end));

%determine offset
proc_result = (negOffset + posOffset) / 2;


function calc_result = calc_amplitude(halfHyst)
%hysteresis size
hystSize = size(halfHyst, 1);
negMax = round(hystSize / 10);
posMin = hystSize - round(hystSize / 5);

%determine amplitude
negAmp = mean(halfHyst(1:negMax));
posAmp = mean(halfHyst(posMin:end));
calc_result = abs(negAmp) + abs(posAmp);


function calc_result = calc_zeroCross(halfHyst, field)
%good, but slow
%check for sign change in kerr effect
calc_result = [];
%smoothHalfHyst = smooth(halfHyst);
smoothHalfHyst = halfHyst;
for i = 1:length(halfHyst)-1
    if sign(smoothHalfHyst(i)) ~= sign(smoothHalfHyst(i+1))
        %find zero crossing with linear approximation (y = mx+c) between negative and
        %positive point
        m = (smoothHalfHyst(i+1)-smoothHalfHyst(i))/(field(i+1)-field(i));
        c = smoothHalfHyst(i) - m*field(i);
        zeroCrossing = -c/m;
        calc_result = [calc_result zeroCrossing];
    end 
end
%if multiple zero crossings are detected set crossing field to NaN
if (size(calc_result,1) ~= 1) || (size(calc_result,2) ~=1)
    calc_result = NaN;
end


function calc_result = proc_remanence(halfHyst, field)
%check for sign change in field
signum = sign(field);
signum(field == 0) = 1;
calc_result = halfHyst(diff(signum) ~= 0);