function varargout = sweep_anal(varargin)
% SWEEP_ANAL MATLAB code for sweep_anal.fig
%      SWEEP_ANAL, by itself, creates a new SWEEP_ANAL or raises the existing
%      singleton*.
%
%      H = SWEEP_ANAL returns the handle to a new SWEEP_ANAL or the handle to
%      the existing singleton*.
%
%      SWEEP_ANAL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SWEEP_ANAL.M with the given input arguments.
%
%      SWEEP_ANAL('Property','Value',...) creates a new SWEEP_ANAL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before sweep_anal_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to sweep_anal_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help sweep_anal

% Last Modified by GUIDE v2.5 09-Jun-2017 09:23:41

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @sweep_anal_OpeningFcn, ...
                   'gui_OutputFcn',  @sweep_anal_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --------------------------------------------------------------------
% GUI Functions
% --------------------------------------------------------------------
% --- Outputs from this function are returned to the command line.
function varargout = sweep_anal_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes just before sweep_anal is made visible.
function sweep_anal_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to sweep_anal (see VARARGIN)

% Choose default command line output for sweep_anal
handles.output = hObject;

%initialise parallelisation and warnings off
wb = waitbar(0, 'Starting up parallelisation...');
%handles.parpool = gcp;
delete(wb)
warning('off','all');

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes sweep_anal wait for user response (see UIRESUME)
% uiwait(handles.figure1);

%load empty images
try
    %Amplitude
    axes(handles.axesAmp)
    imagesc(zeros(512))
    %Remanence
    axes(handles.axesRem)
    imagesc(zeros(512))
    %Coercivity
    axes(handles.axesHc)
    imagesc(zeros(512))
    %Exchange Bias
    axes(handles.axesEb)
    imagesc(zeros(512))
    %Movie
    axes(handles.axesMovie)
    imagesc(zeros(512))
    line(256, 256, 'Marker', '.', 'MarkerSize', 20, 'Color', 'black')
    axes(handles.axesHyst)
    plot(-1200:1:1200,zeros(2401,1));
    line(0, 0, 'Marker', '.', 'MarkerSize', 20, 'Color', 'black')
    pretty_display(handles)
    
catch errLoad
    disp(errLoad)
end

% --- Executes on mouse press over image axes background.
function image_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to axesMovie (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
try
    clickPosition = get(hObject, 'CurrentPoint');
    clickX = round(clickPosition(1,1));
    clickY = round(clickPosition(1,2));
    if (clickX > 512) || (clickY > 512)
        msgbox('You missed the image. Try harder next time!', handles.figure1.Name)
        return
    end
    %Movie
    axes(handles.axesMovie)
    plotHandle = get(gca, 'Children');
    set(plotHandle(1), 'XData', clickX, 'YData', clickY, 'MarkerSize', 20);
    %Hysteresis
    axes(handles.axesHyst)
    plotHandle = get(gca, 'Children');
    curH = get(plotHandle(1), 'XData');
    distanceH = abs((handles.sweep.result.field.Hx - curH)/max(handles.sweep.result.field.Hx));
    curKerr = get(plotHandle(1), 'YData');
    oldKerrData = reshape(get(plotHandle(2), 'YData'),[],1);
    distanceKerr = abs((oldKerrData - curKerr)/max(oldKerrData));
    distance = (distanceH + distanceKerr) / 2;
    minIndex = find(distance == min(distance));
    newKerrData = reshape(handles.sweep.result.hyst(clickY,clickX,:),1,[]);
    plot(handles.sweep.result.field.Hx, newKerrData)
    line(curH, newKerrData(minIndex), 'Marker', '.', 'MarkerSize', 20, 'Color', 'black')
    pretty_display(handles)
catch errImage
    disp(errImage)
end

% --- Executes on mouse press over hysteresis axes background.
function hyst_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to axesMovie (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
try
    clickPosition = get(hObject, 'CurrentPoint');
    clickH = clickPosition(1,1);
    clickKerr = clickPosition(1,2);
    distanceH = abs((handles.sweep.result.field.Hx - clickH)/max(handles.sweep.result.field.Hx));
    plotHandle = get(gca, 'Children');
    kerrData = reshape(get(plotHandle(2), 'YData'),[],1);
    distanceKerr = abs((kerrData - clickKerr)/max(kerrData));
    distance = (distanceH + distanceKerr) / 2;
    minIndex = find(distance == min(distance));
    minHx = handles.sweep.result.field.Hx(minIndex);
    minKerr = kerrData(minIndex);
    %Movie
    axes(handles.axesMovie)
    plotHandle = get(gca, 'Children');
    curPosX = get(plotHandle(1), 'XData');
    curPosY = get(plotHandle(1), 'YData');
    curMarkerSize = get(plotHandle(1), 'MarkerSize');
    imagesc(handles.sweep.result.hyst(:,:,minIndex))
    line(curPosX, curPosY, 'Marker', '.', 'MarkerSize', curMarkerSize, 'Color', 'black')
    %Hysteresis
    axes(handles.axesHyst)
    plotHandle = get(gca, 'Children');
    set(plotHandle(1), 'XData', minHx, 'YData', minKerr, 'MarkerSize', 20);
    pretty_display(handles)
catch errHyst
    disp (errHyst)
end

% --------------------------------------------------------------------
% Menu Functions
% --------------------------------------------------------------------
% --------------------------------------------------------------------
function File_Close_Callback(hObject, eventdata, handles)
% hObject    handle to File_Close (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(handles.figure1)

% --------------------------------------------------------------------
function File_Save_Callback(hObject, eventdata, handles)
% hObject    handle to File_Save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
result = handles.sweep.result;
config = handles.sweep.config;
uisave({'result', 'config'})

% --------------------------------------------------------------------
function File_Open_Proc_Callback(hObject, eventdata, handles)
% hObject    handle to File_Open_Proc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[data_file, data_path] = uigetfile({'*.mat;*.swp','LeXtender Sweep Mapping Acquisition File'});
if data_file == 0
    return
end

%disable all the buttons unless a .swp file is loaded
set(handles.pushbutton_process, 'Enable', 'off');
set(handles.checkboxSpikeFilter, 'Enable', 'off', 'Value', 0);
set(handles.checkboxRecursive, 'Enable', 'off', 'Value', 0);
set(handles.editSpike, 'Enable', 'off', 'String', '1.5');
set(handles.checkboxBinPixels, 'Enable', 'off', 'Value', 0);
set(handles.editBin, 'Enable', 'off', 'String', '1');
set(handles.checkboxEllipse, 'Enable', 'off', 'Value', 0);

%Next line of code used to be
%wb = waitbar(0, 'Loading processing session...', 'Name', handles.msgTitle);
%which produces an error under Matlab 2017. msgTitle seems to get stored
%under handles.figure1.Name. Therefore, every handles.msgTitle was changed
%to handles.figure1.Name.
wb = waitbar(0, 'Loading processing session...', 'Name', handles.figure1.Name);
try
    %load data file
    sweep_file = fullfile(data_path, data_file);
    sweep_load = load(sweep_file, '-MAT');
    
    %put sweep in guidata
    handles.sweep = struct;
    handles.sweep.result = sweep_load.result;
    handles.sweep.config = sweep_load.config;
    
    %clear previous movie
    try
        handles = rmfield(handles, 'movie')
    catch errMovie
        disp(errMovie)
    end
    
    %update guidata
    guidata(hObject, handles);
catch errLoad
    disp(errLoad)
    return
end
show_maps(handles)

set(handles.editSample, 'String', handles.sweep.config.sample);

delete(wb)

% --------------------------------------------------------------------
function File_Open_Sweep_Callback(hObject, eventdata, handles)
% hObject    handle to File_Open_Sweep (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


[data_file, data_path] = uigetfile({'*.mat;*.swp','LeXtender Sweep Mapping Acquisition File'});
if data_file == 0
    return
end

%disable buttons while loading

set(handles.pushbutton_process, 'Enable', 'off');
set(handles.checkboxSpikeFilter, 'Enable', 'off');
set(handles.checkboxBinPixels, 'Enable', 'off');
set(handles.checkboxEllipse, 'Enable', 'off');
pause(0.01)

%try importing data
try
    %load data file
    sweep_file = fullfile(data_path, data_file);
    sweep_load = load(sweep_file, '-MAT');
    
    %put sweep in guidata
    handles.sweep = struct;
    handles.sweep.data = sweep_load.sweep;
    handles.sweep.config = sweep_load.sweep_config;
    
    %clear previous proc
    handles.sweep.result = struct;
    
    %clear previous movie
    try
        handles = rmfield(handles, 'movie')
    catch errMovie
        disp(errMovie)
    end
    
    %update guidata
    guidata(hObject, handles);
catch errLoad
    disp(errLoad)
    return
end

%enable all the buttons unless a .swp file is loaded
set(handles.editSample, 'String', handles.sweep.config.sample);
set(handles.pushbutton_process, 'Enable', 'on');
set(handles.checkboxSpikeFilter, 'Enable', 'on');
set(handles.checkboxBinPixels, 'Enable', 'on');
set(handles.checkboxEllipse, 'Enable', 'on');





% --- Executes on button press in pushbutton_process.
function pushbutton_process_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_process (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%disable all the buttons until the data is processed
set(handles.pushbutton_process, 'Enable', 'off');
set(handles.checkboxSpikeFilter, 'Enable', 'off');
set(handles.checkboxRecursive, 'Enable', 'off');
set(handles.editSpike, 'Enable', 'off');
set(handles.checkboxBinPixels, 'Enable', 'off');
set(handles.editBin, 'Enable', 'off');
set(handles.checkboxEllipse, 'Enable', 'off');

%try processing data
try
    handles.sweep.result = sweep_proc(handles);
    guidata(hObject, handles);
catch errProc
    disp(errProc)
    return
end
show_maps(handles)

%enable all the buttons after data procession
set(handles.pushbutton_process, 'Enable', 'on');
set(handles.checkboxSpikeFilter, 'Enable', 'on');
set(handles.checkboxRecursive, 'Enable', 'on');
set(handles.editSpike, 'Enable', 'on');
set(handles.checkboxBinPixels, 'Enable', 'on');
set(handles.editBin, 'Enable', 'on');
set(handles.checkboxEllipse, 'Enable', 'on');

% --------------------------------------------------------------------
function File_Import_Raster_Callback(hObject, eventdata, handles)
% hObject    handle to File_Import_Raster (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
try
    %import result
    handles.sweep.result = raster_proc;
    
    %clear previous movie
    try
        handles = rmfield(handles, 'movie')
    catch errMovie
        disp(errMovie)
    end
    
    %update guidata
    guidata(hObject, handles);
    show_maps(handles)
catch errImport
    disp(errImport)
end

% --------------------------------------------------------------------
function File_Export_Movie_Callback(hObject, eventdata, handles)
% hObject    handle to File_Export_Movie (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
try
    movie = handles.movie;
catch errMovie
    [myGIF, myAVI, map] = sweep_movie(handles.sweep.result);
    handles.movie = struct;
    handles.movie.GIF = myGIF;
    handles.movie.AVI = myAVI;
    handles.movie.map = map;
    guidata(hObject, handles);
    movie = handles.movie;
end
% Construct a questdlg with three options
choice = questdlg('Chose file format to save movie', handles.figure1.Name, 'AVI','GIF','surprise me','surprise me');
% Handle response
switch choice
    case 'AVI'
        format = 0;
    case 'GIF'
        format = 1;
    case 'surprise me'
        format = randi([0 1]);
end
switch format
    case 0
        try
            [fileName, filePath] = uiputfile('*.avi', 'AVI Video', 'Save AVI Video');
            movie2avi(movie.AVI, [filePath fileName], 'compression', 'None');
        catch errSave
            if strcmp(choice, 'surprise me')
                msgbox('You wanted to be surprised!', handles.figure1.Name)
            end
        end
    case 1
        try
            [fileName, filePath] = uiputfile('*.gif', 'GIF Image', 'Save GIF Image');
            imwrite(movie.GIF, movie.map, [filePath fileName], 'DelayTime', 0, 'LoopCount', inf);
        catch errSave
            if strcmp(choice, 'surprise me')
                msgbox('You wanted to be surprised!', handles.figure1.Name)
            end
        end
end

% --------------------------------------------------------------------
function View_Reset_Callback(hObject, eventdata, handles)
% hObject    handle to View_Reset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
show_maps(handles)

% --------------------------------------------------------------------
function View_Movie_Callback(hObject, eventdata, handles)
% hObject    handle to View_Movie (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[myGIF, myAVI, map] = sweep_movie(handles.sweep.result);
handles.movie = struct;
handles.movie.GIF = myGIF;
handles.movie.AVI = myAVI;
handles.movie.map = map;
guidata(hObject, handles);

% --------------------------------------------------------------------
function Edit_DeleteRaw_Callback(hObject, eventdata, handles)
% hObject    handle to Edit_DeleteRaw (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.sweep.result.raw = -1;
guidata(hObject, handles);


% --------------------------------------------------------------------
% General Functions
% --------------------------------------------------------------------
% --------------------------------------------------------------------
function show_maps(handles)
%show maps
try
    axes(handles.axesAmp)
    imagesc(handles.sweep.result.maps.Amp)
    axes(handles.axesRem)
    imagesc(handles.sweep.result.maps.Rem)
    axes(handles.axesHc)
    imagesc(handles.sweep.result.maps.Hc)
    axes(handles.axesEb)
    imagesc(handles.sweep.result.maps.Eb)
    axes(handles.axesHyst)
    plot(handles.sweep.result.field.Hx, handles.sweep.result.average.procHyst)
    line(handles.sweep.result.field.Hx(1,1), handles.sweep.result.average.procHyst(1,1), 'Marker', '.', 'MarkerSize', 20, 'Color', 'black')
    axes(handles.axesMovie)
    imagesc(handles.sweep.result.hyst(:,:,1))
    line(0, 0, 'Marker', '.', 'MarkerSize', 1, 'Color', 'black')
    pretty_display(handles)
catch errDisp
    disp(errDisp)
end

function pretty_display(handles)
%load empty images
%Amplitude
axes(handles.axesAmp)
title('Amplitude')
colorbar
set(gca,'xtick',[])
set(gca,'xticklabel',[])
set(gca,'ytick',[])
set(gca,'yticklabel',[])
plotHandle = get(gca, 'Children');
for i=1:size(plotHandle)
    set(plotHandle(i), 'HitTest', 'off')
    set(gca,'ButtonDownFcn','sweep_anal(''image_ButtonDownFcn'',gca,''eventdata'',guidata(gca))')
end
%Remanence
axes(handles.axesRem)
title('Remanence')
colorbar
set(gca,'xtick',[])
set(gca,'xticklabel',[])
set(gca,'ytick',[])
set(gca,'yticklabel',[])
plotHandle = get(gca, 'Children');
for i=1:size(plotHandle)
    set(plotHandle(i), 'HitTest', 'off')
    set(gca,'ButtonDownFcn','sweep_anal(''image_ButtonDownFcn'',gca,''eventdata'',guidata(gca))')
end
%Coercivity
axes(handles.axesHc)
title('Coercivity')
colorbar
set(gca,'xtick',[])
set(gca,'xticklabel',[])
set(gca,'ytick',[])
set(gca,'yticklabel',[])
plotHandle = get(gca, 'Children');
for i=1:size(plotHandle)
    set(plotHandle(i), 'HitTest', 'off')
    set(gca,'ButtonDownFcn','sweep_anal(''image_ButtonDownFcn'',gca,''eventdata'',guidata(gca))')
end
%Exchange Bias
axes(handles.axesEb)
title('Exchange Bias')
colorbar
set(gca,'xtick',[])
set(gca,'xticklabel',[])
set(gca,'ytick',[])
set(gca,'yticklabel',[])
plotHandle = get(gca, 'Children');
for i=1:size(plotHandle)
    set(plotHandle(i), 'HitTest', 'off')
    set(gca,'ButtonDownFcn','sweep_anal(''image_ButtonDownFcn'',gca,''eventdata'',guidata(gca))')
end
%Movie
axes(handles.axesMovie)
title('Kerr Rotation')
set(gca,'xtick',[])
set(gca,'xticklabel',[])
set(gca,'ytick',[])
set(gca,'yticklabel',[])
plotHandle = get(gca, 'Children');
for i=1:size(plotHandle)
    set(plotHandle(i), 'HitTest', 'off')
end
set(gca,'ButtonDownFcn','sweep_anal(''image_ButtonDownFcn'',gca,''eventdata'',guidata(gca))')
%Hysteresis
axes(handles.axesHyst)
plotHandle = get(gca, 'Children');
for i=1:size(plotHandle)
    set(plotHandle(i), 'HitTest', 'off')
end
dataX = get(plotHandle(2), 'XData');
xlim([min(dataX) max(dataX)])
set(gca,'ButtonDownFcn','sweep_anal(''hyst_ButtonDownFcn'',gca,''eventdata'',guidata(gca))')


% --- Executes on button press in checkboxSpikeFilter.
function checkboxSpikeFilter_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxSpikeFilter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxSpikeFilter

if (get(hObject,'Value') == get(hObject,'Max'))
    set(handles.checkboxRecursive,'Enable','on')
    set(handles.editSpike,'Enable','on')
else
    set(handles.checkboxRecursive,'Enable','off')
    set(handles.editSpike,'Enable','off')
end

% --- Executes on button press in checkboxBinPixels.
function checkboxBinPixels_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxBinPixels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxBinPixels

if (get(hObject,'Value') == get(hObject,'Max'))
    set(handles.editBin,'Enable','on')
else
    set(handles.editBin,'Enable','off')
end


% --- Executes on button press in pushbuttonEnable.
function pushbuttonEnable_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonEnable (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.pushbutton_process, 'Enable', 'on');
set(handles.checkboxSpikeFilter, 'Enable', 'on');
set(handles.checkboxBinPixels, 'Enable', 'on');
set(handles.checkboxEllipse, 'Enable', 'on');


% --- Executes on button press in checkboxEllipse.
function checkboxEllipse_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxEllipse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxEllipse
