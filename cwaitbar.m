%CoffeeBar is a purely legendary awesome replacement for the waitbar
%filling a coffee cup instead of increasing a red bar
%Joachim Gr�fe 2013
function varargout = cwaitbar(varargin)
if nargin > 1
    if ishandle(varargin{2})
        handle = varargin{2};
    else
        handle = createFigure;
    end
else
    handle = createFigure;
end
switch nargin
    %one input --> new CoffeeBar at user value
    case 1
        tassenIndex = chooseIndex(handle, varargin{1});
        updateImage(handle, tassenIndex)
    case 2
        %check if handle is provided, else assume new CoffeeBar with
        %text
        tassenIndex = chooseIndex(handle, varargin{1});
        updateImage(handle, tassenIndex)
        if ~ishandle(varargin{2})
        	updateText(handle, varargin{2})
        end
    %update CoffeeBar value and text    
    case 3
        tassenIndex = chooseIndex(handle, varargin{1});
        updateImage(handle, tassenIndex)
        updateText(handle, varargin{3})
    otherwise
        if nargin > 0
            if ~ishandle(varargin{2})
                updateText(handle, varargin{2})
            end
            if ~strcmp(varargin{3},'Name')
                updateText(handle, varargin{3})
            end
            updateTitle(handle, varargin{end})
        end
end
if nargout > 0
    varargout{1} = handle;
end
pause(0.01)

%create a CoffeeBar at zero value
function handle = createFigure
handle = figure;
%intialize empty figure
set(handle, 'Color', 'white', 'Resize', 'off', 'ToolBar', 'none', 'MenuBar', 'none', 'NumberTitle','off')
%resize figure
figPosition = get(handle, 'Position');
figPosition(3) = 320;
figPosition(4) = 240;
set(handle, 'Position', figPosition)
%load images as appdata
loadImages(handle)
tassen = getappdata(handle, 'Tassen');
%show empty cup
imagesc(tassen(1).image)
%intialize axis
hAxis = get(handle, 'Children');
set(hAxis,'XTick',[], 'XTickLabel',[],'YTick',[],'YTickLabel',[], 'Visible', 'off')

%load image data into appdata of the CoffeeBar
function loadImages(handle)
load('tassen.mat')
setappdata(handle, 'Tassen', tassen)

%update image shown in the CoffeeBar
function updateImage(handle, tassenIndex)
tassen = getappdata(handle, 'Tassen');
hAxis = get(handle, 'Children');
hImage = get(hAxis, 'Children');
set(hImage, 'CData', tassen(tassenIndex).image);

%update CoffeeBar title
function updateTitle(handle, title)
set(handle, 'Name', title)

%update coffeeBar text
function updateText(handle, cText)
hAxis = get(handle, 'Children');
hTitle = title(hAxis, cText);
set(hTitle, 'Visible', 'on')

%determine index of image to display by fraction
function tassenIndex = chooseIndex(handle, X)
tassen = getappdata(handle, 'Tassen');
maxIndex = size(tassen,2);
tassenIndex = round(X*maxIndex);
if tassenIndex < 1
    tassenIndex = 1;
end
if tassenIndex > maxIndex
    tassenIndex = maxIndex;
end