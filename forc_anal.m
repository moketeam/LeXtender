function varargout = forc_anal(varargin)
% FORC_ANAL MATLAB code for forc_anal.fig
%      FORC_ANAL, by itself, creates a new FORC_ANAL or raises the existing
%      singleton*.
%
%      H = FORC_ANAL returns the handle to a new FORC_ANAL or the handle to
%      the existing singleton*.
%
%      FORC_ANAL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FORC_ANAL.M with the given input arguments.
%
%      FORC_ANAL('Property','Value',...) creates a new FORC_ANAL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before forc_anal_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to forc_anal_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help forc_anal

% Last Modified by GUIDE v2.5 25-Aug-2017 14:04:03
try
    % Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @forc_anal_OpeningFcn, ...
                       'gui_OutputFcn',  @forc_anal_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    % End initialization code - DO NOT EDIT
catch errGUI
    disp(errGUI)
end


% --- Executes just before forc_anal is made visible.
function forc_anal_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to forc_anal (see VARARGIN)

%initialise processing cache
handles.proc = struct;

% Choose default command line output for forc_anal
handles.output = struct;
handles.output.control = 1;

% Update handles structure
guidata(hObject, handles);

%initialise parallelisation and warnings off
wb = waitbar(0, 'Starting up parallelisation...');
handles.parpool = gcp;
delete(wb)
warning('off','all');

% UIWAIT makes forc_anal wait for user response (see UIRESUME)
if nargin > 3
    uiwait(handles.figure1);
else
    handles.output.control = -1;
    guidata(hObject, handles);
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
if isequal(get(hObject, 'waitstatus'), 'waiting')
    % The GUI is still in UIWAIT, us UIRESUME
    uiresume(hObject);
else
    % The GUI is no longer waiting, just close it
    delete(hObject);
end


% --- Outputs from this function are returned to the command line.
function varargout = forc_anal_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% The figure can be deleted now
if handles.output.control ~= -1
    delete(handles.figure1);
end


% --------------------------------------------------------------------
% UI Functions
% --------------------------------------------------------------------
function uiInterpolation_Callback(hObject, eventdata, handles)
% hObject    handle to uiInterpolation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of uiInterpolation as text
%        str2double(get(hObject,'String')) returns contents of uiInterpolation as a double
hx_field = handles.forc.config.fieldsHx;
interpol_steps = str2num(get(handles.uiInterpolation, 'String'));
interpol_field = 2 * hx_field / interpol_steps;
set(hObject, 'TooltipString', ['Interpolating to ', num2str(interpol_field), ' Oe'])


function uiSF_Callback(hObject, eventdata, handles)
% hObject    handle to uiSF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of uiSF as text
%        str2double(get(hObject,'String')) returns contents of uiSF as a double
hx_field = handles.forc.config.fieldsHx;
interpol_steps = str2num(get(handles.uiInterpolation, 'String'));
smoothing_factor = str2num(get(handles.uiSF, 'String'));
if smoothing_factor > interpol_steps
    set(hObject, 'String', num2str(interpol_steps))
end
smoothing_field = 2 * hx_field / interpol_steps * smoothing_factor;
set(hObject, 'TooltipString', ['Smoothing ', num2str(smoothing_field), ' Oe'])


function uiHbCutOff_Callback(hObject, eventdata, handles)
% hObject    handle to uiHbCutOff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of uiHbCutOff as text
%        str2double(get(hObject,'String')) returns contents of uiHbCutOff as a double
hb_cutoff = str2num(get(handles.uiHbCutOff, 'String'));
if (hb_cutoff < 0)
    set(hObject, 'String', '20')
end


% --- Executes on button press in uiProcess.
function uiProcess_Callback(hObject, eventdata, handles)
% hObject    handle to uiProcess (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%load processing configuration
interpol_steps = str2num(get(handles.uiInterpolation, 'String'));
smoothing_factor = str2num(get(handles.uiSF, 'String'));
hb_cutoff = str2num(get(handles.uiHbCutOff, 'String'));

%look for reprocessing
reproIS = 0;
for i=1:size(handles.proc,2)
    try
        if interpol_steps == handles.proc(i).interpol_steps
            reproIS(end+1) = i;
        end
    catch errRepro
        disp(errRepro)
    end
end
reproSF = 0;
for i=reproIS
    if i ~= 0
        if smoothing_factor == handles.proc(i).smoothing_factor
            reproSF(end+1) = i;
        end
    end
end
repro = 0;
for i=reproSF
    if i~= 0
        if hb_cutoff == handles.proc(i).hb_cutoff
            repro(end+1) = i;
        end
    end
end
if size(repro,2) > 1
    i = repro(2);
    msgbox([handles.proc(i).forc_name, ' already processed!'])
    return
end

%process FORC
output = forc_proc(handles.forc.data, interpol_steps, smoothing_factor, hb_cutoff);


%make this FORC current FORC
proc_name = ['I=', num2str(interpol_steps), ', SF=', num2str(smoothing_factor), ', HbCO=', num2str(hb_cutoff)];
handles.proc(1).output = output;
handles.proc(1).interpol_steps = interpol_steps;
handles.proc(1).smoothing_factor = smoothing_factor;
handles.proc(1).hb_cutoff = hb_cutoff;
handles.proc(1).proc_name = proc_name;
%save FORC to guidata
forc_num = size(handles.proc, 2) + 1;
handles.proc(forc_num).output = output;
handles.proc(forc_num).interpol_steps = interpol_steps;
handles.proc(forc_num).smoothing_factor = smoothing_factor;
handles.proc(forc_num).hb_cutoff = hb_cutoff;
handles.proc(forc_num).proc_name = proc_name;
%update guidata
guidata(hObject, handles);

%update processed list and display current result
ui_update_list(handles)
ui_show_forc(handles)


% --- Executes on button press in uiShowProc.
function uiShowProc_Callback(hObject, eventdata, handles)
% hObject    handle to uiShowProc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
new_proc = get(handles.uiProcList, 'Value');
if new_proc ~= 1
    handles.proc(1).output = handles.proc(new_proc).output;
    handles.proc(1).interpol_steps = handles.proc(new_proc).interpol_steps;
    handles.proc(1).smoothing_factor = handles.proc(new_proc).smoothing_factor;
    handles.proc(1).hb_cutoff = handles.proc(new_proc).hb_cutoff;
    handles.proc(1).proc_name = handles.proc(new_proc).proc_name;
    guidata(hObject, handles);
end
ui_update_list(handles)
ui_show_forc(handles)


% --- Executes on button press in uiOpenFigurePM.
function uiOpenFigureM_Callback(hObject, eventdata, handles)
% hObject    handle to uiOpenFigurePM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
figure('Name', [handles.proc(1).proc_name ' - Magnetisation'])
colormap(jet);
surf(handles.proc(1).output.Hb, handles.proc(1).output.Ha, handles.proc(1).output.M, 'EdgeColor', 'none')
xlabel('Hb (Oe)')
ylabel('Ha (Oe)')
zlabel('M (mdeg)')
   

% --- Executes on button press in uiOpenFigureM.
function uiOpenFigurePM_Callback(hObject, eventdata, handles)
% hObject    handle to uiOpenFigureM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
figure('Name', [handles.proc(1).proc_name ' - FORC Density'])
colormap(jet);
surf(handles.proc(1).output.Hc, handles.proc(1).output.Hu, handles.proc(1).output.pM, 'EdgeColor', 'none')
view(2);
xlabel('Hc (Oe)')
ylabel('Hu (Oe)')
zlabel('P') 



% --- Executes on button press in uiOpenFigureForcs.
function uiOpenFigureForcs_Callback(hObject, eventdata, handles)
% hObject    handle to uiOpenFigureForcs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
figure('Name', [handles.proc(1).proc_name ' - FORCs'])
colormap(jet);
for i=1:handles.proc(1).interpol_steps
    pM = handles.proc(1).output.pM(i,:);
    Hb = handles.proc(1).output.Hb;
    M = handles.proc(1).output.M(i,:);
    surf([Hb(:), Hb(:)], [M(:), M(:)], [pM(:), pM(:)], [pM(:), pM(:)],'EdgeColor','flat', 'FaceColor','none');
    hold on;
end
view(2);
xlabel('Hb (Oe)')
ylabel('Ha (Oe)')

% --------------------------------------------------------------------
% Menu Functions
% --------------------------------------------------------------------
% --------------------------------------------------------------------
function File_Open_Forc_Callback(hObject, eventdata, handles)
% hObject    handle to File_Open_Forc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%let user select data file
[data_file, data_path] = uigetfile({'*.mat;*.forc','LeXtender FORC Acquisition File'});
if data_file == 0
    return
end

try
    %load data file
    forc_file = fullfile(data_path, data_file);
    forc_load = load(forc_file, '-MAT');
    
    %put forc in guidata
    handles.forc = struct;
    handles.forc.data = forc_load.forc;
    handles.forc.config = forc_load.forc_config;
    
    %clear previous proc
    handles.proc = struct;
    
    %update guidata
    guidata(hObject, handles);
catch errLoad
    disp(errLoad)
    return
end

%show file config and recommend processing settings
ui_show_info(handles)
ui_update_list(handles)


% --------------------------------------------------------------------
function File_Open_Proc_Callback(hObject, eventdata, handles)
% hObject    handle to File_Open_Proc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[data_file, data_path] = uigetfile({'*.mat','LeXtender FORC Processing File'});
if data_file == 0
    return
end
try
    %load data file
    proc_file = fullfile(data_path, data_file);
    proc_load = load(proc_file, '-MAT');
    
    %put session in guidata
    handles.forc = struct;
    handles.forc = proc_load.forc;
    handles.proc = struct;
    handles.proc = proc_load.proc;
    
    %update guidata
    guidata(hObject, handles);
    
    %update list and show FORC
    ui_show_info(handles)
    ui_update_list(handles)
    ui_show_forc(handles)
catch errLoad
    disp(errLoad)
end


% --------------------------------------------------------------------
function File_Save_Callback(hObject, eventdata, handles)
% hObject    handle to File_Save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
proc = handles.proc;
forc = handles.forc;
uisave({'proc', 'forc'})


% --------------------------------------------------------------------
function Close_Callback(hObject, eventdata, handles)
% hObject    handle to Close (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(handles.figure1)

% --------------------------------------------------------------------
function Edit_Cut_Callback(hObject, eventdata, handles)
% hObject    handle to Edit_Cut (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%save current processing session
try
if questdlg('Cuting the FORC will overwrite the FORC data and invalidate the previous processing results. Do you want to save the current processing session?','FORC Analyser','Yes','No','Yes') == 'Yes'
	proc = handles.proc;
    forc = handles.forc;
    uisave({'proc', 'forc'})
end
end

%ask for cut off limits
forc = handles.forc;
maxHx = num2str(forc.config.fieldsHx);
inputLimit = inputdlg({'Cut at amplitude of reversal field Ha', 'Cut at minor loop field Hb'},'FORC Analyser',1,{maxHx,maxHx});
if size(inputLimit,1) == 0
    return
end
nForc = forc_cut(forc, str2num(inputLimit{1}), str2num(inputLimit{2}));
%put forc in guidata
handles.forc = nForc;  
%clear previous proc
handles.proc = struct;    
%update guidata
guidata(hObject, handles);

%show file config and recommend processing settings
ui_show_info(handles)
ui_update_list(handles)


% --------------------------------------------------------------------
function Analyse_Reversibility_Callback(hObject, eventdata, handles)
% hObject    handle to Analyse_Reversibility (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% rework GUI to reflect that curren HbCutOff value is taken into account
% for calculation of the reversibility plot

forc = handles.forc;
hbCutOff = str2num(get(handles.uiHbCutOff, 'String')); %%%%%%
output = forc_revers(forc.data, hbCutOff); % Shreyas VSM special option!!!
%output = forc_revers(forc.data);
figure('Name', 'LeXtender FORC Analyser - Reversibility')
[AX,H1,H2] = plotyy(output.H, output.sMajor, output.H(1:end-1), output.sRev);
xlabel('H (Oe)')
xlim(AX(1), [min(output.H) max(output.H)])
xlim(AX(2), [min(output.H) max(output.H)])
ylim(AX(1), [min(output.sMajor)*1.1 max(output.sMajor)*1.1])
ylim(AX(2), [-0.05 1.05]);
set(get(AX(1), 'Ylabel'), 'String', 'M (mdeg)');
set(get(AX(2), 'Ylabel'), 'String', 'R');
set(AX(2),'NextPlot','add')
scatter(AX(2), output.H(1:end-1), output.sRev, 5, get(H2, 'Color'))


% --------------------------------------------------------------------
% Reusable Functions
% --------------------------------------------------------------------
% FORC meta data
% --------------------------------------------------------------------
function hx_step = info_hx_step(forc, config)
%calculate Ha fields
fieldsHa = NaN(size(forc,2));
fieldsHx = config.fieldsHx;
parfor i=1:size(forc,2)
    fieldsHa(i) = forc(i).fieldHa * fieldsHx;
end
fieldsHa = sort(fieldsHa);

%find smallest Ha step
hx_step = abs(config.fieldsHx);
for i=1:(size(fieldsHa,1)-1)
    hx_diff = abs(fieldsHa(i+1) - fieldsHa(i));
    if hx_diff < hx_step
        hx_step = hx_diff;
    end
end


% --------------------------------------------------------------------
% UI helper
% --------------------------------------------------------------------
function ui_update_list(handles)
myList{1} = 'CURRENT';
for i=2:size(handles.proc, 2)
    myList{i} = handles.proc(i).proc_name;
end
set(handles.uiProcList, 'String', myList)
set(handles.uiProcList, 'Value', 1)

function ui_show_forc(handles)
%display M
axes(handles.axesM)
surf(handles.proc(1).output.Hb, handles.proc(1).output.Ha, handles.proc(1).output.M, 'EdgeColor', 'none')
title(handles.proc(1).proc_name)
xlabel('Hb (Oe)')
ylabel('Ha (Oe)')
zlabel('M (mdeg)')

%display pM
axes(handles.axesPM)
surf(handles.proc(1).output.Hc, handles.proc(1).output.Hu, handles.proc(1).output.pM, 'EdgeColor', 'none')
view(2);
title(handles.proc(1).proc_name)
xlabel('Hc (Oe)')
ylabel('Hu (Oe)')
zlabel('P')

%display Forcs
axes(handles.axesForcs)
hold on;
cla
for i=1:handles.proc(1).interpol_steps
    pM = handles.proc(1).output.pM(i,:);
    Hb = handles.proc(1).output.Hb;
    M = handles.proc(1).output.M(i,:);
    surf([Hb(:), Hb(:)], [M(:), M(:)], [pM(:), pM(:)], [pM(:), pM(:)],'EdgeColor','flat', 'FaceColor','none');
end
view(2);
grid on
title(handles.proc(1).proc_name)
xlabel('Hb (Oe)')
ylabel('Ha (Oe)')
hold off;

function ui_show_info(handles)
hx_field = handles.forc.config.fieldsHx;
hx_step = info_hx_step(handles.forc.data, handles.forc.config);
interpol_steps = round(2 * hx_field / hx_step);
interpol_field = 2 * hx_field / interpol_steps;
smoothing_factor = round(interpol_steps/20);
if smoothing_factor == 0
    smoothing_factor = 1;
end
smoothing_field = 2 * hx_field / interpol_steps * smoothing_factor;
set(handles.uiSample, 'String', handles.forc.config.sample);
set(handles.uiFieldAmplitude, 'String', num2str(hx_field));
set(handles.uiFieldStep, 'String', num2str(hx_step));
set(handles.uiInterpolation, 'String', num2str(interpol_steps));
set(handles.uiInterpolation, 'TooltipString', ['Interpolating to ', num2str(interpol_field), ' Oe'])
set(handles.uiSF, 'String', num2str(smoothing_factor));
set(handles.uiSF, 'TooltipString', ['Smoothing over ', num2str(smoothing_field), ' Oe'])
set(handles.uiHbCutOff, 'String', '20');



