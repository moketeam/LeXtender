%------------------------------------------------------------------------
%forc major loop reconstruction
%inputs: forc(filename or forc struct array), HbCutOff, fSF, wb_title
%------------------------------------------------------------------------
function output = forc_major(varargin)
%start time
tic;

proc_config = struct();
switch nargin
    case 1
        proc_config.forc = varargin{1};
        proc_config.HbCutOff = 20;
        proc_config.fSF = 0.05;
        proc_config.title = 'LeXtender FORC Major Loop Reconstruction';
        proc_config.standAlone = 0;
    case 2
        proc_config.forc = varargin{1};
        proc_config.HbCutOff = varargin{2};
        proc_config.fSF = 0.05;
        proc_config.title = 'LeXtender FORC Major Loop Reconstruction';
        proc_config.standAlone = 0;
    case 3
        proc_config.forc = varargin{1};
        proc_config.HbCutOff = varargin{2};
        proc_config.fSF = varargin{3};
        proc_config.title = 'LeXtender FORC Major Loop Reconstruction';
        proc_config.standAlone = 0;       
    case 4
        proc_config.forc = varargin{1};
        proc_config.HbCutOff = varargin{2};
        proc_config.fSF = varargin{3};
        proc_config.title = varargin{4};
        proc_config.standAlone = 0; 
    otherwise
        [data_file, data_path] = uigetfile({'*.mat;*.forc','LeXtender FORC Acquisition File'});
        if data_file == 0
            return
        end
        proc_config.forc = fullfile(data_path, data_file);
        proc_config.HbCutOff = 20;
        proc_config.fSF = 0.05;
        proc_config.title = 'LeXtender FORC Major Loop Reconstruction';
        proc_config.standAlone = 1;
end

%%load forc data and intialise settings
wb_title = proc_config.title;
wb = cwaitbar(0, 'Initialising...', 'Name', wb_title);
%load forc data
if ischar(proc_config.forc) == 1
    loadVariables = load(proc_config.forc, '-mat');
    forc = loadVariables.forc;
    %forc_config = loadVariables.forc_config;
else
    forc = proc_config.forc;
end
%dataset size
numHa = size(forc,2);

%read data from forc
cwaitbar(0, wb, 'Processing major hysteresis...', 'Name', wb_title);
Hf = forc(1).data(proc_config.HbCutOff:end,1);
Mf = forc(1).data(proc_config.HbCutOff:end,2);
for i=1:numHa
    cwaitbar(i/numHa, wb);
    Hr(i) = forc(i).data(proc_config.HbCutOff,1);
    Mr(i) = forc(i).data(proc_config.HbCutOff,2);
end
Mr = smooth(Mr, proc_config.fSF);
delete(wb)

%return data
rStart = size(Hf, 1) + 1;
rStop = rStart + size(Hr, 2) - 1;
output.H = Hf;
output.M = Mf;
output.H(rStart:rStop) = Hr;
output.M(rStart:rStop) = Mr;

%show data if in stand alone mode
if proc_config.standAlone == 1
    figure('Name', proc_config.forc)
    plot(output.H, output.M)
end