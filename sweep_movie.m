function [myGIF, myAVI, map] = sweep_movie(result)
%creating figure
fig = figure('Color', 'white', 'Position', [100 100 400 800]);
subplot(2,1,1);
myImage = imagesc(result.hyst(:,:,1), [min(result.average.procHyst) max(result.average.procHyst)]);
set(gca,'xtick',[])
set(gca,'xticklabel',[])
set(gca,'ytick',[])
set(gca,'yticklabel',[])
subplot(2,1,2);
plot(result.field.Hx, result.average.procHyst)
xlabel('Magnetic Field / Oe');
ylabel('Kerr Rotation / mdeg');
myMarker = line(result.field.Hx(1,1), result.average.procHyst(1,1), 'Marker', '.', 'MarkerSize', 20, 'Color', 'b');

%creating movie
movieSize = size(result.field.Hx, 1);
myGIF = zeros(800, 400, 1, movieSize, 'uint8');
myAVI(1) = getframe(gcf);
[myGIF(:,:,1,1), map] = rgb2ind(myAVI(1).cdata, 256, 'nodither');
for i=2:movieSize
    set(myMarker, 'XData', result.field.Hx(i,1), 'YData', result.average.procHyst(i,1));
    set(myImage, 'CData', result.hyst(:,:,i));
    myAVI(i) = getframe(gcf);
    myGIF(:,:,1,i) = rgb2ind(myAVI(i).cdata, map, 'nodither');
end
%movie2avi(myAVI, 'myMovie.avi', 'compression', 'None');
%imwrite(myGIF, map, 'myMovie.gif', 'DelayTime', 0, 'LoopCount', inf);