function varargout = sweep_acq(varargin)
% sweep_acq MATLAB code for sweep_acq.fig
%      sweep_acq, by itself, creates a new sweep_acq or raises the existing
%      singleton*.
%
%      H = sweep_acq returns the handle to a new sweep_acq or the handle to
%      the existing singleton*.
%
%      sweep_acq('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in sweep_acq.M with the given input arguments.
%
%      sweep_acq('Property','Value',...) creates a new sweep_acq or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before sweep_acq_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to sweep_acq_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help sweep_acq

% Last Modified by GUIDE v2.5 11-Feb-2013 15:43:30
try
    % Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @sweep_acq_OpeningFcn, ...
                       'gui_OutputFcn',  @sweep_acq_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    % End initialization code - DO NOT EDIT
catch errGUI
    disp(errGUI)
end


%------------------------------------------------------------------------
%initialisation functions
%------------------------------------------------------------------------
% --- Executes just before sweep_acq is made visible.
function sweep_acq_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to sweep_acq (see VARARGIN)

% Choose default command line output for sweep_acq
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

%load config
try
    config = load('config.mat');
    set(handles.uiResolution, 'String', config.sweep_acq.fieldsHxRes);
    set(handles.uiField, 'String', config.sweep_acq.fieldsHx);
    set(handles.uiFieldOffset, 'String', config.sweep_acq.fieldsHxOffset);
catch errLoad
    disp(errLoad)
end

% --- Outputs from this function are returned to the command line.
function varargout = sweep_acq_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


%------------------------------------------------------------------------
%menu functions
%------------------------------------------------------------------------
% --------------------------------------------------------------------
function Close_Callback(hObject, eventdata, handles)
% hObject    handle to Close (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(sweep_acq)

% --------------------------------------------------------------------
function settings_customfields_Callback(hObject, eventdata, handles)
% hObject    handle to settings_customfields (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fieldsHaRes = str2num(get(handles.uiResolution, 'String'));
fieldsHx = str2num(get(handles.uiField, 'String'));
fieldsHxOffset = str2num(get(handles.uiFieldOffset, 'String'));
[result data] = customfields(fieldsHx, fieldsHaRes, fieldsHxOffset);
if (result == 1)
    if isnan(data(2,1))
        msgbox('Use main instrument settings for specifing linear field steps')
    else
        %determine overall field amplitude/offset
        max_field = max(max(data));
        min_field = min(min(data));
        field_offset = abs(max_field + min_field)/2;
        field_amplitude = abs(max_field - field_offset);
        
        %write to interface
        set(handles.uiResolution, 'String', '0');
        set(handles.uiField, 'String', num2str(field_amplitude));
        set(handles.uiFieldOffset, 'String', num2str(field_offset));
        
        %save customfields
        save('customfields.mat', 'data');
    end
end
    

%------------------------------------------------------------------------
%main functions
%------------------------------------------------------------------------
%select data folder
%------------------------------------------------------------------------
% --- Executes on button press in uiOpenFolder.
function uiOpenFolder_Callback(hObject, eventdata, handles)
% hObject    handle to uiOpenFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data_folder = uigetdir();
set(handles.uiFolder, 'String', data_folder)

%------------------------------------------------------------------------
%test acquisition parameters
%------------------------------------------------------------------------
% --- Executes on button press in uiTest.
function uiTest_Callback(hObject, eventdata, handles)
% hObject    handle to uiTest (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%read UI values
data_folder = get(handles.uiFolder, 'String');

%check data folder
if isempty(data_folder)
    msgbox('Please select Data Folder')
    return
end
img_file = fullfile(data_folder, '\test.txt');
if exist(img_file, 'file') ~= 0
    delete(img_file)
end

%ask user to prepare LXPro
waitfor(msgbox('Please set Imaging Display to Kerr rotation and Fields to Manual fields', 'LeXtender Sweep Acquisition'))

%generate AutoMOKE commands
headerString = automoke_header(0);
photoString = automoke_photo;
exportString = automoke_photo_export(data_folder, 'test');
output = char(headerString, photoString, exportString);

%write AutoMOKE file and wait for execution
write_automoke('LeXtender_test.txt', output)
while check_automoke('LeXtender_test.txt') == 0
    pause(1)
end

%load test data and delete files
import_img = import_photo(data_folder, 'test');
delete(strcat(data_folder, '\test.txt'));

%show test photo
axes(handles.axes1)
imagesc(import_img)
title('Test Photo')

%------------------------------------------------------------------------
%acquire data
%------------------------------------------------------------------------
% --- Executes on button press in uiAcquire.
function uiAcquire_Callback(hObject, eventdata, handles)
% hObject    handle to uiAcquire (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%read UI values
sweep_config = struct;
sweep_config.data_folder = get(handles.uiFolder, 'String');
sweep_config.sample = get(handles.uiSample, 'String');
sweep_config.fieldsHxRes = str2num(get(handles.uiResolution, 'String'));
sweep_config.fieldsHx = str2num(get(handles.uiField, 'String'));
sweep_config.fieldsHxOffset = str2num(get(handles.uiFieldOffset, 'String'));

%check data folder
if isempty(sweep_config.data_folder)
    msgbox('Please select Data Folder')
    return
end

%inialize data storage and calculate Hx steps from - to +
sweep = struct;
if sweep_config.fieldsHxRes ~= 0
    %linear fields
    num_fields = sweep_config.fieldsHxRes + 1;
    for i=1:num_fields
        j = i - 1;
        sweep(i).fieldHx = ((-1 + (j / sweep_config.fieldsHxRes) * 2) * sweep_config.fieldsHx) + sweep_config.fieldsHxOffset;
        sweep(i).direction = 1;
    end
else
    %%%custom fields
    %%load custom fields
    sweep_config.customfields = struct;
    sweep_config.customfields = load('customfields.mat');
    
    %%calculate fields
    num_fields = 0;
    %for each line in customfields
    for i = 1:10
        %ignore NaN lines
        if sweep_config.customfields.data(i,1) ~= NaN
            lower_limit = sweep_config.customfields.data(i,1);
            upper_limit = sweep_config.customfields.data(i,3);
            custom_step = sweep_config.customfields.data(i,2);
            
            %step through values
            current_field = lower_limit;
            while current_field <= upper_limit
                num_fields = num_fields + 1;
                sweep(num_fields).fieldHx = (current_field - sweep_config.fieldsHxOffset);
                sweep(num_fields).direction = 1;
                current_field = current_field + custom_step;
            end
        end
    end
end
%calculate Hx steps from + to -
for i=1:num_fields
    iField = 1 + num_fields - i;
    j = num_fields + i;
    sweep(j).fieldHx = sweep(iField).fieldHx;
    sweep(j).direction = -1;
end
%now there is twice the number of fields
num_fields = num_fields * 2;

%ask user to prepare LXPro
waitfor(msgbox('Please set Imaging Display to Kerr rotation and Fields to Manual fields', 'LeXtender Sweep Acquisition'))

%%%%%%%%%%%%%%%%%%%%%%%%%
%measure field by field %
%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:num_fields
    %filenames for current Hx
    current_data_file = strcat('sweep_', num2str(sweep(i).direction), '_data_', num2str(sweep(i).fieldHx));
    automoke_file = strcat('sweep_', num2str(sweep(i).direction), '_', num2str(sweep(i).fieldHx), '.txt');
    
    %generate AutoMOKE commands
    headerString = automoke_header(sweep(i).fieldHx);
    photoString = automoke_photo;
    exportString = automoke_photo_export(sweep_config.data_folder, current_data_file);
    output = char(headerString, photoString, exportString);
    
    %write AutoMOKE file and wait for execution
    write_automoke(automoke_file, output)
    while check_automoke(automoke_file) == 0
        pause(1)
    end
    
    %load photo
    sweep(i).rawData = import_photo(sweep_config.data_folder, current_data_file);
    
    %show current photo
    axes(handles.axes1)
    imagesc(sweep(i).rawData)
    title(strcat(num2str(i), '/', num2str(num_fields), ' Photo Acquisition'))
    
    %store filenames and acquisition time
    sweep(i).filenames.data = current_data_file;
    sweep(i).time = datestr(now, 'dd.mm.yyyy - HH:MM:SS');
end

%write acquisition file
filename_base = strcat(sweep_config.data_folder, '\', sweep_config.sample, '_', datestr(now, 'yyyy-mm-dd_HHMM'));
sweep_config.acq_file = strcat(filename_base, '.swp');
save(sweep_config.acq_file, 'sweep_config', 'sweep');

%zip files
zip_filelist = {};
for i=1:size(sweep,2)
    zip_filelist{end+1} = strcat(sweep_config.data_folder, '\', sweep(i).filenames.data, '.txt');
end
zip_filename = strcat(filename_base, '.zip');
zip(zip_filename, zip_filelist)
for i=1:size(zip_filelist, 2)
    delete(zip_filelist{i})
end

%idle NanoMOKE
resetString = automoke_reset;
write_automoke('fieldshap_reset.txt', resetString);

%data acquisition done
msgbox(strcat('Acquisition of ', num2str(num_fields), ' Photos successful'), 'LeXtender Sweep Mapping Acquisition')


%------------------------------------------------------------------------
%reusable functions
%------------------------------------------------------------------------
%generate AutoMOKE commands
%------------------------------------------------------------------------
function returnString = automoke_header(fields_hx)
fields_hx_str = num2str(fields_hx);
line1 = 'laser_on()';
line2 = strcat('set_fields_hx(', fields_hx_str, ')');
returnString = char(line1, line2);

function returnString = automoke_reset
line1 = 'stop_fields()';
line2 = 'laser_off()';
returnString = char(line1, line2);

function returnString = automoke_photo
line1 = 'start_fields()';
line2 = 'start_photo()';
returnString = char(line1, line2);

function returnString = automoke_photo_export(data_folder, filename)
line1 = strcat('export_image_text_NI(', data_folder, '\', filename, '.txt)');
returnString = char(line1);

%------------------------------------------------------------------------
%import data
%------------------------------------------------------------------------
function importImage = import_photo(data_folder, filename)
filename = strcat(filename, '.txt');
image_file = fullfile(data_folder, filename);
importImage = importdata(image_file);

function finished = check_automoke(filename)
automoke_file = strcat('C:\NanoMOKEInbox\', filename);
if exist(automoke_file, 'file') == 0
    finished = 1;
else
    finished = 0;
end

%------------------------------------------------------------------------
%export data
%------------------------------------------------------------------------
function write_automoke(filename, output)
%automoke_file = strcat('C:\', filename)
automoke_file = filename;
inbox_file = strcat('C:\NanoMOKEInbox\', filename);
fileID = fopen(automoke_file, 'wt');
for i=1:size(output,1)
   fprintf(fileID, '%s\n', output(i,:));
end
fclose(fileID);
movefile(automoke_file, inbox_file)