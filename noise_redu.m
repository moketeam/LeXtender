function forc_redu = noise_redu(forc,threshold)
    del = find(forc(:,6)>threshold);
    j = size(del);
for k=1:j
    forc(del(k)-k+1,:) = []; % why should you use forc(del(k)-k+1,:) = [];
end
forc_redu = forc;
end