function varargout = customfields(varargin)
% CUSTOMFIELDS MATLAB code for customfields.fig
%      CUSTOMFIELDS by itself, creates a new CUSTOMFIELDS or raises the
%      existing singleton*.
%
%      H = CUSTOMFIELDS returns the handle to a new CUSTOMFIELDS or the handle to
%      the existing singleton*.
%
%      CUSTOMFIELDS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CUSTOMFIELDS.M with the given input arguments.
%
%      CUSTOMFIELDS('Property','Value',...) creates a new CUSTOMFIELDS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before customfields_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to customfields_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help customfields

% Last Modified by GUIDE v2.5 11-Feb-2013 15:11:15
try
    % Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @customfields_OpeningFcn, ...
                       'gui_OutputFcn',  @customfields_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    % End initialization code - DO NOT EDIT
catch errGUI
    disp(errGUI)
end

% --- Executes just before customfields is made visible.
function customfields_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to customfields (see VARARGIN)

% Choose default command line output for customfields
handles.output = struct();
handles.output.result = 0;
handles.output.fields = NaN(10,3);

% Update handles structure
guidata(hObject, handles);

% Show a question icon from dialogicons.mat - variables questIconData
% and questIconMap
load dialogicons.mat

IconData=questIconData;
questIconMap(256,:) = get(handles.figure1, 'Color');
IconCMap=questIconMap;

Img=image(IconData, 'Parent', handles.axes1);
set(handles.figure1, 'Colormap', IconCMap);

set(handles.axes1, ...
    'Visible', 'off', ...
    'YDir'   , 'reverse'       , ...
    'XLim'   , get(Img,'XData'), ...
    'YLim'   , get(Img,'YData')  ...
    );

% setup initial data
if nargin == (3+3)
    if varargin{2} == 0
        loadVars = load('customfields.mat');
        inData = loadVars.data;
    else
        inData = NaN(10,3);
        inData(1,1) = -1*varargin{1}+varargin{3};
        inData(1,2) = 2*varargin{1}/varargin{2};
        inData(1,3) = varargin{1}+varargin{3};
    end
    set(handles.uitable1,'data',inData);
end

% Make the GUI modal
set(handles.figure1,'WindowStyle','modal')

% UIWAIT makes customfields wait for user response (see UIRESUME)
uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = customfields_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output.result;
varargout{2} = handles.output.fields;

% The figure can be deleted now
delete(handles.figure1);

% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.output.result = 1;
handles.output.fields = get(handles.uitable1,'data');

% Update handles structure
guidata(hObject, handles);

% Use UIRESUME instead of delete because the OutputFcn needs
% to get the updated handles structure.
uiresume(handles.figure1);

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.output.result = 0;

% Update handles structure
guidata(hObject, handles);

% Use UIRESUME instead of delete because the OutputFcn needs
% to get the updated handles structure.
uiresume(handles.figure1);

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if isequal(get(hObject, 'waitstatus'), 'waiting')
    % The GUI is still in UIWAIT, us UIRESUME
    uiresume(hObject);
else
    % The GUI is no longer waiting, just close it
    delete(hObject);
end
