function varargout = lextender(varargin)
% LEXTENDER MATLAB code for lextender.fig
%      LEXTENDER, by itself, creates a new LEXTENDER or raises the existing
%      singleton*.
%
%      H = LEXTENDER returns the handle to a new LEXTENDER or the handle to
%      the existing singleton*.
%
%      LEXTENDER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LEXTENDER.M with the given input arguments.
%
%      LEXTENDER('Property','Value',...) creates a new LEXTENDER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before lextender_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to lextender_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help lextender

% Last Modified by GUIDE v2.5 20-Feb-2015 16:53:17
try
    % Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @lextender_OpeningFcn, ...
                       'gui_OutputFcn',  @lextender_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    % End initialization code - DO NOT EDIT
catch errGUI
    disp(errGUI)
end

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%close parpool if open
delete(gcp('nocreate'))

% Hint: delete(hObject) closes the figure
delete(hObject);


%------------------------------------------------------------------------
%initialisation functions
%------------------------------------------------------------------------
% --- Executes just before acq_main is made visible.
function lextender_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to lextender (see VARARGIN)

% Choose default command line output for lextender
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = lextender_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


%------------------------------------------------------------------------
%menu functions
%------------------------------------------------------------------------
% --------------------------------------------------------------------
function File_Close_Callback(hObject, eventdata, handles)
% hObject    handle to File_Close (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(lextender)

% --------------------------------------------------------------------
function Mapping_Raster_Processing_Callback(hObject, eventdata, handles)
% hObject    handle to Mapping_Raster_Processing (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
acq_proc

% --------------------------------------------------------------------
function Mapping_Raster_Acquisition_Callback(hObject, eventdata, handles)
% hObject    handle to Mapping_Raster_Acquisition (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
acq_main

% --------------------------------------------------------------------
function Mapping_Sweep_Acquisition_Callback(hObject, eventdata, handles)
% hObject    handle to Mapping_Sweep_Acquisition (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
sweep_acq

% --------------------------------------------------------------------
function Mappaing_Sweep_Analyse_Callback(hObject, eventdata, handles)
% hObject    handle to Mappaing_Sweep_Analyse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
sweep_anal

% --------------------------------------------------------------------
function FORC_Acquisition_Callback(hObject, eventdata, handles)
% hObject    handle to FORC_Acquisition (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
forc_acq

% --------------------------------------------------------------------
function FORC_Processing_Callback(hObject, eventdata, handles)
% hObject    handle to FORC_Processing (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
forc_proc

% --------------------------------------------------------------------
function FORC_Analyse_Callback(hObject, eventdata, handles)
% hObject    handle to FORC_Analyse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
forc_anal

% --------------------------------------------------------------------
function Convert_mok2txt_Callback(hObject, eventdata, handles)
% hObject    handle to Convert_mok2txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
mok2txt


% --------------------------------------------------------------------
function FORC_Recovery_Callback(hObject, eventdata, handles)
% hObject    handle to FORC_Recovery (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
forc_recover


% --------------------------------------------------------------------
function FORC_VSM_Callback(hObject, eventdata, handles)
% hObject    handle to FORC_VSM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
forc_vsm_gui
