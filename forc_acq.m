function varargout = forc_acq(varargin)
% forc_acq MATLAB code for forc_acq.fig
%      forc_acq, by itself, creates a new forc_acq or raises the existing
%      singleton*.
%
%      H = forc_acq returns the handle to a new forc_acq or the handle to
%      the existing singleton*.
%
%      forc_acq('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in forc_acq.M with the given input arguments.
%
%      forc_acq('Property','Value',...) creates a new forc_acq or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before forc_acq_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to forc_acq_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help forc_acq

% Last Modified by GUIDE v2.5 25-Apr-2017 14:54:21
try
    % Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @forc_acq_OpeningFcn, ...
                       'gui_OutputFcn',  @forc_acq_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    % End initialization code - DO NOT EDIT
catch errGUI
    disp(errGUI)
end


%------------------------------------------------------------------------
%initialisation functions
%------------------------------------------------------------------------
% --- Executes just before forc_acq is made visible.
function forc_acq_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to forc_acq (see VARARGIN)

% Choose default command line output for forc_acq
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

%load config
try
    config = load('config.mat');
    set(handles.uiResolutionA, 'String', config.forc_acq.fieldsHaRes);
    set(handles.uiField, 'String', config.forc_acq.fieldsHx);
    set(handles.uiFieldOffset, 'String', config.forc_acq.fieldsHxOffset);
    set(handles.uiFrequency, 'String', config.forc_acq.frequency);
    set(handles.uiAveraging, 'String', config.forc_acq.grab_max);
catch errLoad
    disp(errLoad)
end

% --- Outputs from this function are returned to the command line.
function varargout = forc_acq_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


%------------------------------------------------------------------------
%menu functions
%------------------------------------------------------------------------
% --------------------------------------------------------------------
function Close_Callback(hObject, eventdata, handles)
% hObject    handle to Close (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(forc_acq)

% --------------------------------------------------------------------
function settings_customfields_Callback(hObject, eventdata, handles)
% hObject    handle to settings_customfields (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fieldsHaRes = str2num(get(handles.uiResolutionA, 'String'));
fieldsHx = str2num(get(handles.uiField, 'String'));
fieldsHxOffset = str2num(get(handles.uiFieldOffset, 'String'));
[result data] = customfields(fieldsHx, fieldsHaRes, fieldsHxOffset);
if (result == 1)
    if isnan(data(2,1))
        msgbox('Use main instrument settings for specifing linear field steps.')
    else
        %Just in case someone fucks up the custom field settings
        for i=1:10
            if(data(i,1))>=(data(i,3))
                msgbox('Custom field choice might not make sense.')
                break
            end
        end
        
        %determine overall field amplitude/offset
        max_field = max(max(data));
        min_field = min(min(data));
        field_offset = abs(max_field + min_field)/2;
        field_amplitude = abs(max_field - field_offset);
        
        %write to interface
        set(handles.uiResolutionA, 'String', '0');
        set(handles.uiField, 'String', num2str(field_amplitude));
        set(handles.uiFieldOffset, 'String', num2str(field_offset));
        
        %save customfields
        save('customfields.mat', 'data');
    end
end
    

%------------------------------------------------------------------------
%main functions
%------------------------------------------------------------------------
%select data folder
%------------------------------------------------------------------------
% --- Executes on button press in uiOpenFolder.
function uiOpenFolder_Callback(hObject, eventdata, handles)
% hObject    handle to uiOpenFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data_folder = uigetdir();
set(handles.uiFolder, 'String', data_folder)
acq_file = strcat(data_folder, '\acquistion.forc');
if exist(acq_file) ~= 0
    msgbox('The selected data folder already contains data. Upon acquisition this data will be overwritten!', 'LeXtender FORC Acquisition', 'warn')
end

%------------------------------------------------------------------------
%test acquisition parameters
%------------------------------------------------------------------------
% --- Executes on button press in uiTest.
function uiTest_Callback(hObject, eventdata, handles)
% hObject    handle to uiTest (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%read UI values
data_folder = get(handles.uiFolder, 'String');
fields_hx = str2num(get(handles.uiField, 'String'));
fields_hx_res = str2num(get(handles.uiResolutionB, 'String'));
fields_hx_offset = str2num(get(handles.uiFieldOffset, 'String'));
fields_frequency = str2num(get(handles.uiFrequency, 'String'));
grab_max = str2num(get(handles.uiAveraging, 'String'));

%check data folder
if isempty(data_folder)
    msgbox('Please select Data Folder')
    return
end
hyst_file = strcat(data_folder, '\test.txt');
if exist(hyst_file, 'file') ~= 0
    delete(hyst_file)
end

%write FORC field profile
write_forc_profile(data_folder, 'testfield', -1, fields_hx_res)

%generate AutoMOKE commands
headerString = automoke_header(fields_hx, fields_hx_offset, fields_frequency, grab_max);
fieldString = automoke_fieldshape(data_folder, 'testfield');
measureString = automoke_measure;
exportString = automoke_export(data_folder, 'test');
output = char(headerString, fieldString, measureString, exportString);

%write AutoMOKE file and wait for execution
write_automoke('LeXtender_test.txt', output)
while check_automoke('LeXtender_test.txt') == 0
    pause(1)
end

%load test data and delete files
import_array = import_hysteresis(data_folder, 'test');
delete(strcat(data_folder, '\testfield.txt'));
delete(strcat(data_folder, '\test.txt'));

%show test FORC
forc_array = forc_extract(import_array.data);
axes(handles.axes1)
plot(forc_array(:,1), forc_array(:,2))
title('Test Acquisition FORC')
xlabel(import_array.colheaders(1,3))
ylabel(import_array.colheaders(1,5))

%test FORC acquisition parameters
forc_test_parameters(import_array.data);

%------------------------------------------------------------------------
%acquire data
%------------------------------------------------------------------------
% --- Executes on button press in uiAcquire.
function uiAcquire_Callback(hObject, eventdata, handles)
% hObject    handle to uiAcquire (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%read UI values
forc_config = struct;
forc_config.data_folder = get(handles.uiFolder, 'String');
forc_config.sample = get(handles.uiSample, 'String');
forc_config.fieldsHaRes = str2num(get(handles.uiResolutionA, 'String'));
forc_config.fieldsHbRes = str2num(get(handles.uiResolutionB, 'String'));
forc_config.fieldsHx = str2num(get(handles.uiField, 'String'));
forc_config.fieldsHxOffset = str2num(get(handles.uiFieldOffset, 'String'));
forc_config.frequency = str2num(get(handles.uiFrequency, 'String'));
forc_config.grab_max = str2num(get(handles.uiAveraging, 'String'));

%calculate power limiting time
forc_config.cool_time = round(forc_config.grab_max / forc_config.frequency);

%inialize data storage and calculate Ha steps
forc = struct;
if forc_config.fieldsHaRes ~= 0
    %linear fields
    num_fields = forc_config.fieldsHaRes + 1;
    for i=1:num_fields
        j = i - 1;
        forc(i).fieldHa = -1 + (j / forc_config.fieldsHaRes) * 2;
    end
else
    %%%custom fields
    %%load custom fields
    forc_config.customfields = struct;
    forc_config.customfields = load('customfields.mat');
    
    %%calculate fields
    num_fields = 0;
    %for each line in customfields
    for i = 1:10
        %ignore NaN lines
        if forc_config.customfields.data(i,1) ~= NaN
            lower_limit = forc_config.customfields.data(i,1);
            upper_limit = forc_config.customfields.data(i,3);
            custom_step = forc_config.customfields.data(i,2);
            
            %step through values
            current_field = lower_limit;
            while current_field <= upper_limit
                num_fields = num_fields + 1;
                %round field values, so x*e-16 does not occur, only works
                %with 64 bit system so we use ghetto fix for now which
                %works with every matlab version
                %forc(num_fields).fieldHa = round((current_field - forc_config.fieldsHxOffset) / forc_config.fieldsHx, 10);
                forc(num_fields).fieldHa = round(10000000000*(current_field - forc_config.fieldsHxOffset) / forc_config.fieldsHx)/10000000000;
                current_field = current_field + custom_step;
            end
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%
%measure FORC by FORC%
%%%%%%%%%%%%%%%%%%%%%%
for i=1:num_fields
    %here is a lazy fix for reexecuting minor loop acquisitons. When the
    %LXPro software crashes it doesn't wirte data files. Error handling
    %is just trying again for 5 times.
    success = false;
    counter = 1;
    while success ~= true %danger, infinte loop
        counter = counter + 1;
        try
            %filenames for current Ha
            current_profile_file = strcat('forc_profile_', num2str(forc(i).fieldHa));
            current_data_file = strcat('forc_data_', num2str(forc(i).fieldHa));
            automoke_file = strcat('forc_', num2str(forc(i).fieldHa), '.txt');
            
            %write FORC field profile
            write_forc_profile(forc_config.data_folder, current_profile_file, forc(i).fieldHa, forc_config.fieldsHbRes)
            
            %generate AutoMOKE commands
            headerString = automoke_header(forc_config.fieldsHx, forc_config.fieldsHxOffset, forc_config.frequency, forc_config.grab_max);
            fieldString = automoke_fieldshape(forc_config.data_folder, current_profile_file);
            measureString = automoke_measure;
            exportString = automoke_export(forc_config.data_folder, current_data_file);
            coolString = automoke_pause(forc_config.cool_time);
            output = char(headerString, fieldString, measureString, exportString, coolString);
            
            %write AutoMOKE file and wait for execution
            write_automoke(automoke_file, output)
            while check_automoke(automoke_file) == 0
                pause(1)
            end
            
            %load FORC
            forc(i).rawData = import_hysteresis(forc_config.data_folder, current_data_file);
            forc(i).data = forc_extract(forc(i).rawData.data);
            
            %show current FORC
            axes(handles.axes1)
            plot(forc(i).data(:,1), forc(i).data(:,2))
            title(strcat(num2str(i), '/', num2str(num_fields), ' FORC Acquisition'))
            xlabel(forc(i).rawData.colheaders(1,3))
            ylabel(forc(i).rawData.colheaders(1,5))
            
            %store filenames and acquisition time
            forc(i).filenames.data = current_data_file;
            forc(i).filenames.profile = current_profile_file;
            forc(i).time = datestr(now, 'dd.mm.yyyy - HH:MM:SS');
            
            %leave infint loop
            success = true;
        catch errMeasure
            disp errMeasure
        end
        if counter > 5
            if success ~= true
                msgbox('Suff failed to execute', 'Lazy FORC Fix')
            end
            success = true;
        end
    end
end

%write acquisition file
filename_base = strcat(forc_config.data_folder, '\', forc_config.sample, '_', datestr(now, 'yyyy-mm-dd_HHMM'));
forc_config.acq_file = strcat(filename_base, '.forc');
save(forc_config.acq_file, 'forc_config', 'forc');

%zip files and rename archive to .forc
zip_filelist = {};
for i=1:size(forc,2)
    zip_filelist{end+1} = strcat(forc_config.data_folder, '\', forc(i).filenames.data, '.txt');
    zip_filelist{end+1} = strcat(forc_config.data_folder, '\', forc(i).filenames.profile, '.txt');
end
zip_filename = strcat(filename_base, '.zip');
zip(zip_filename, zip_filelist)
for i=1:size(zip_filelist, 2)
    delete(zip_filelist{i})
end

%reset field shape
resetString = automoke_filedshape_reset;
write_automoke('fieldshap_reset.txt', resetString);

%data acquisition done
msgbox(strcat('Acquisition of ', num2str(num_fields), ' FORCs successful'), 'LeXtender FORC Acquisition')


%------------------------------------------------------------------------
%reusable functions
%------------------------------------------------------------------------
%FORC functions
%------------------------------------------------------------------------
function hysteresis = forc_extract(data)
%positive und negative S�ttigung fitten
sat_neg = polyfit(data(106:140,3), data(106:140,5), 1);
sat_pos = polyfit(data(206:240,3), data(206:240,5), 1);
slope = mean([sat_neg(1); sat_pos(1)]);
offset = mean([sat_neg(2); sat_pos(2)]);
numHb = size(data,1);% + 400;
for i=401:numHb
    hx = data(i,3);
    kerr = data(i,5);
    corr_kerr = kerr - offset - slope * hx;
    j = i - 400;
    hyst(j,:) = [hx, corr_kerr];
end
hysteresis = hyst;

function result = forc_test_parameters(data)
tempResult = 1;
message = {};
%Feldstabilit�t pr�fen
%negative S�ttigung
field_negSat = data(56:90,3);
field_negSat_mean = mean(field_negSat);
field_negSat_std = std(field_negSat);
field_negSat = abs(field_negSat_std / field_negSat_mean);
if field_negSat > 0.0015
    tempResult = 0;
    message{end+1} = 'negative saturation field not stabilised';
end
%90% der positiven S�ttigung
field_posSat = data(176:195,3);
field_posSat_mean = mean(field_posSat);
field_posSat_std = std(field_posSat);
field_posSat = abs(field_posSat_std / field_posSat_mean);
if field_posSat > 0.0049
    tempResult = 0;
    message{end+1} = 'positive saturation field not stabilised';
end
%Ha - Umkehrfeld
field_Ha = data(351:395,3);
field_Ha_mean = mean(field_Ha);
field_Ha_std = std(field_Ha);
field_Ha = abs(field_Ha_std / field_Ha_mean);
if field_Ha > 0.0015
    tempResult = 0;
    message{end+1} = 'reversal field not stabilised';
end

%S�ttigung �berpr�fen
sat_neg = polyfit(data(106:140,3), data(106:140,5), 1);
sat_pos = polyfit(data(206:240,3), data(206:240,5), 1);
%Steigung
sat_slope = [sat_neg(1); sat_pos(1)];
sat_slope_mean = mean(sat_slope);
sat_slope_std = std(sat_slope);
sat_slope = abs(sat_slope_std / sat_slope_mean);
if sat_slope > 0.5
    tempResult = 0;
    message{end+1} = 'slope correction failed';
elseif sat_slope > 0.1
    message{end+1} = 'slope correction inaccurate';
end
sat_offset = [sat_neg(2); sat_pos(2)];
sat_offset_mean = mean(sat_offset);
sat_offset_std = std(sat_offset);
sat_offset = abs(sat_offset_std / sat_offset_mean);

%output mesage and return result
if size(message) > 0
    msgbox(message)
end
result = tempResult

%------------------------------------------------------------------------
%generate AutoMOKE commands
%------------------------------------------------------------------------
function returnString = automoke_header(fields_hx, fields_hx_offset, fields_frequency, grab_max)
fields_hx_str = num2str(fields_hx);
fields_hx_offset_str = num2str(fields_hx_offset);
fields_frequency_str = num2str(fields_frequency);
grab_max_str = num2str(grab_max);
line1 = 'laser_on()';
line2 = strcat('set_fields_hx(', fields_hx_str, ')');
line3 = strcat('set_fields_hxoffset(', fields_hx_offset_str, ')');
line4 = strcat('set_fields_frequency(', fields_frequency_str, ')');
line5 = strcat('set_grab_maxaverages(', grab_max_str, ')');
line6 = strcat('clear_process()');
returnString = char(line1, line2, line3, line4, line5, line6);

function returnString = automoke_fieldshape(data_folder, filename)
line1 = strcat('set_fields_importfilename(', data_folder, '\', filename, '.txt)');
line2 = strcat('set_fields_hxshape(5)');
returnString = char(line1, line2);

function returnString = automoke_reset
line1 = strcat('set_fields_hxshape(1)');
line2 = 'laser_off()';
returnString = char(line1, line2);

function returnString = automoke_measure
line1 = 'start_grab()';
line2 = 'wait_grab_finished()';
returnString = char(line1, line2);

function returnString = automoke_export(data_folder, filename)
line1 = strcat('export_all_loop_text_NI(', data_folder, '\', filename, '.txt)');
returnString = char(line1);

function returnString = automoke_pause(pause_time)
line1 = strcat('wait(', num2str(pause_time), ')');
returnString = char(line1);

%------------------------------------------------------------------------
%import data
%------------------------------------------------------------------------
function importArray = import_processed(data_folder, filename)
proc_file = strcat(data_folder, '\', filename, '_proc.txt');
delimiter = '=';
tempArray = importdata(proc_file, delimiter);
for i=1:4
    tempArray.textdata(i,1) = strtrim(tempArray.textdata(i,1));
end
importArray = tempArray;

function importArray = import_hysteresis(data_folder, filename)
hyst_file = strcat(data_folder, '\', filename, '.txt');
delimiter = '\t';
header = 1;
importArray = importdata(hyst_file, delimiter, header);

function finished = check_automoke(filename)
automoke_file = strcat('C:\NanoMOKEInbox\', filename);
if exist(automoke_file, 'file') == 0
    finished = 1;
else
    finished = 0;
end

%------------------------------------------------------------------------
%export data
%------------------------------------------------------------------------
function write_automoke(filename, output)
%automoke_file = strcat('C:\', filename)
automoke_file = filename;
inbox_file = strcat('C:\NanoMOKEInbox\', filename);
fileID = fopen(automoke_file, 'wt');
for i=1:size(output,1)
   fprintf(fileID, '%s\n', output(i,:));
end
fclose(fileID);
movefile(automoke_file, inbox_file)

function write_forc_profile(data_folder, filename, Ha, HbSteps)
%generate profile
fieldspan = 1 - Ha;
numFields = 400 + HbSteps;
for i=1:numFields
    if i < 151
        %150 Messpunkte um negative S�ttigung aufzunehmen
        j = i;
        if j < 101
            %negative S�ttigung stabilisieren
            field(i) = -1;
        elseif j < 151
            %Steigung von -1 bis -0.9 aufnehmen
            k = j - 100;
            field(i) = -1 + 0.1*(k/50);
        end
    elseif i < 301
        %150 Messpunke um positive S�ttigung aufzunehmen
        j = i - 150;
        if j < 51
            %90% der positive S�ttigung stabilisieren
            field(i) = 0.9;
        elseif j < 101
            %Steigung von 0.9 bis 1 aufnehmen
            k = j - 50;
            field(i) = 0.9 + 0.1*(k/50);
        elseif j < 151
            %positive S�ttigung aufnehmen
            field(i) = 1;
        end
    elseif i < 401
        %100 Messpunkte um Ha stabil anzufahren
        j = i - 300;
        field(i) = Ha;
    else
        %restliche Messpunkte f�r den Bereich Ha bis 1
        %lineare Feldschritte zwischen Ha und positiver S�ttigung
        j = i - 400;
        field(i) = Ha + fieldspan*(j/HbSteps);
    end
end

%write profile to file
forc_file = strcat(data_folder, '\', filename, '.txt');
dlmwrite(forc_file, field(:), 'newline', 'pc')
