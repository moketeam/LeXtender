function acq_recover
%import acquisition file
[data_file, data_folder] = open_dialog;
acq_file = importdata(strcat(data_folder, data_file), ',', 6);

%load header data
header = acq_file.textdata;

%find processed hystereses
files = dir([data_folder,'\y*_x*_proc.txt']);
for i=1:size(files,1)
    remainStr = strtok(files(i).name, 'y');
    [xValue,remainStr] = strtok(remainStr, '_x');
    yValue = strtok(remainStr, '_x');
    data(i,1) = str2num(yValue);
    data(i,2) = str2num(xValue);
end

%write recover.txt
write_acquisition(data_folder,header,data)

%------------------------------------------------------------------------
%open file dialog
%------------------------------------------------------------------------
function [data_file, data_path] = open_dialog
[data_file, data_path] = uigetfile({'acquistion.txt;acquisition.txt','LeXtender Acquisition File'});
if data_file ~= 0
    if exist(data_file, 'file') ~= 0
        msgbox('No valid file selected.', 'LeXtender Processing')
        clear data_file
        clear data_path
    end
else
    msgbox('No file selected.', 'LeXtender Processing')
    clear data_file
    clear data_path
end

function write_acquisition(data_folder, header, data)
acq_file = strcat(data_folder, '\recover.txt');
fileID = fopen(acq_file, 'wt');
for i=1:size(header,1)
    fprintf(fileID, '%s\n', header{i,1});
end
for j=1:size(data,1)
    outStr = strcat(num2str(data(j,1)), ',', num2str(data(j,2)));
    fprintf(fileID, '%s\n', outStr);
end
fclose(fileID);