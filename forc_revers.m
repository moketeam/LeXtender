%------------------------------------------------------------------------
%forc reversibility analysis
%inputs: forc(filename or forc struct array), HbCutOff, fSF, wb_title
%------------------------------------------------------------------------
function output = forc_revers(varargin)
%start time
tic;

%take variable input
proc_config = struct();
switch nargin
    case 1
        proc_config.forc = varargin{1};
        proc_config.HbCutOff = 20;
        proc_config.fSF = 0.05;
        proc_config.title = 'LeXtender FORC Reversibility Analysis';
        proc_config.standAlone = 0;
    case 2
        proc_config.forc = varargin{1};
        proc_config.HbCutOff = varargin{2};
        proc_config.fSF = 0.05;
        proc_config.title = 'LeXtender FORC Reversibility Analysis';
        proc_config.standAlone = 0;
    case 3
        proc_config.forc = varargin{1};
        proc_config.HbCutOff = varargin{2};
        proc_config.fSF = varargin{3};
        proc_config.title = 'LeXtender FORC Reversibility Analysis';
        proc_config.standAlone = 0;
    case 4
        proc_config.forc = varargin{1};
        proc_config.HbCutOff = varargin{2};
        proc_config.fSF = varargin{3};
        proc_config.title = varargin{4};
        proc_config.standAlone = 0;
    otherwise
        [data_file, data_path] = uigetfile({'*.mat;*.forc','LeXtender FORC Acquisition File'});
        if data_file == 0
            return
        end
        proc_config.forc = fullfile(data_path, data_file);
        proc_config.HbCutOff = 20;
        proc_config.fSF = 0.05;
        proc_config.title = 'LeXtender FORC Reversibility Analysis';
        proc_config.standAlone = 1;
end

%%load forc data and intialise settings
wb_title = proc_config.title;
wb = cwaitbar(0, 'Initialising...', 'Name', wb_title);
%load forc data
if ischar(proc_config.forc) == 1
    loadVariables = load(proc_config.forc, '-mat');
    forc = loadVariables.forc;
    %forc_config = loadVariables.forc_config;
else
    forc = proc_config.forc;
end
%dataset size
numHa = size(forc,2);

%read data from forc
cwaitbar(0, wb, 'Processing major hysteresis...', 'Name', wb_title);
for i=1:numHa
    cwaitbar(i/numHa, wb);
    H(i) = forc(i).data(proc_config.HbCutOff,1);
    M(i) = forc(i).data(proc_config.HbCutOff,2);
end
sM = smooth(M, proc_config.fSF);

%find forc route
cwaitbar(0, wb, 'Processing forcs...', 'Name', wb_title);
for i=1:numHa-1
    cwaitbar(i/numHa, wb);
    Xmajor(i) = (sM(i+1) - sM(i)) / (H(i+1) - H(i));
    if Xmajor(i) < 0.0001
        Xmajor(i) = 0.0001;
    end
    curFind = find(forc(i).data(proc_config.HbCutOff:end,1) > H(i+1), 1, 'first');
    if ~isempty(curFind)
        Hforc(i) = forc(i).data(curFind,1);
        sForc = smooth(forc(i).data(:,2), proc_config.fSF);
        Mforc(i) = sForc(curFind);
        Xforc(i) = (Mforc(i) - sM(i)) / (Hforc(i) - H(i));
        if Xforc(i) < 0.0001
            Xforc(i) = 0.0001;
        end
    else
        Xforc(i) = 0.0001;
    end
end

%smooth data
Xmajor(Xmajor < proc_config.fSF*max(Xmajor)) = 0.0001;
sXmajor = smooth(Xmajor, proc_config.fSF);
sXmajor(sXmajor < proc_config.fSF*max(sXmajor)) = 0.0001;
Xforc(Xforc < proc_config.fSF*max(Xforc)) = 0.0001;
sXforc = smooth(Xforc, proc_config.fSF);
sXforc(sXforc < proc_config.fSF*max(sXforc)) = 0.0001;

%calculate reversibility
Rev = ones(numHa-1, 1);
Rev = sXforc ./ sXmajor;
Rev(Rev > 1) = 1;
Rev(Rev < 0) = 0;
Rev(isnan(sXforc)) = 1;
Rev(isnan(Rev)) = 1;
sRev = smooth(Rev, proc_config.fSF);

%smooth data and differentiate
%rHa(:,2) = diff(sMHa(:,2));
%tempHa(:,1:2) = rHa(startHa:stopHa,1:2);

%return data
output.H = H;
output.Mmajor = M;
output.sMajor = sM;
output.Rev = Rev;
output.sRev =  sRev;
delete(wb)

%show data if in stand alone mode
if proc_config.standAlone == 1
    figure('Name', proc_config.forc)
    [AX,H1,H2] = plotyy(H, sM, H(1:end-1), sRev);
    xlabel('H (Oe)')
    xlim(AX(1), [min(H) max(H)])
    xlim(AX(2), [min(H) max(H)])
    ylim(AX(1), [min(sM)*1.1 max(sM)*1.1])
    ylim(AX(2), [-0.05 1.05]);
    set(get(AX(1), 'Ylabel'), 'String', 'M (mdeg)');
    set(get(AX(2), 'Ylabel'), 'String', 'R');
    set(AX(2),'NextPlot','add')
    scatter(AX(2), H(1:end-1), sRev, 5, get(H2, 'Color'))
end

%%% nested functions
%%noise reduction
    function forc_redu = noise_redu(forc,threshold)
        del = find(forc(:,5)>threshold);
        j = size(del);
        for k=1:j
            forc(del(k)-k+1,:) = [];
        end
        forc_redu = forc;
    end

end