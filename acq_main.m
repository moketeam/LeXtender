function varargout = acq_main(varargin)
% acq_main MATLAB code for acq_main.fig
%      acq_main, by itself, creates a new acq_main or raises the existing
%      singleton*.
%
%      H = acq_main returns the handle to a new acq_main or the handle to
%      the existing singleton*.
%
%      acq_main('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in acq_main.M with the given input arguments.
%
%      acq_main('Property','Value',...) creates a new acq_main or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before acq_main_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to acq_main_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help acq_main

% Last Modified by GUIDE v2.5 01-Feb-2013 19:00:07
try
    % Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @acq_main_OpeningFcn, ...
                       'gui_OutputFcn',  @acq_main_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    % End initialization code - DO NOT EDIT
catch errGUI
    disp(errGUI)
end


%------------------------------------------------------------------------
%initialisation functions
%------------------------------------------------------------------------
% --- Executes just before acq_main is made visible.
function acq_main_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to acq_main (see VARARGIN)

% Choose default command line output for acq_main
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

%load config
try
    config = load('config.mat');
    set(handles.uiPosition, 'String', config.acq_main.ul_position);
    set(handles.uiResolution, 'String', config.acq_main.scan_resolution);
    set(handles.uiField, 'String', config.acq_main.fields_hx);
    set(handles.uiFrequency, 'String', config.acq_main.fields_frequency);
    set(handles.uiAveraging, 'String', config.acq_main.grab_max);
    set(handles.uiHolding, 'String', config.acq_main.wait_time);
catch errLoad
    disp(errLoad)
end

% --- Outputs from this function are returned to the command line.
function varargout = acq_main_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


%------------------------------------------------------------------------
%menu functions
%------------------------------------------------------------------------
% --------------------------------------------------------------------
function Close_Callback(hObject, eventdata, handles)
% hObject    handle to Close (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(acq_main)

% --------------------------------------------------------------------
function ProcessOpen_Callback(hObject, eventdata, handles)
% hObject    handle to ProcessOpen (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
acq_proc

% --------------------------------------------------------------------
function ProcessCurrent_Callback(hObject, eventdata, handles)
% hObject    handle to ProcessCurrent (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data_folder = strcat(get(handles.uiFolder, 'String'), '\');
data_file = 'acquisition.txt';
acq_proc(data_file, data_folder)


%------------------------------------------------------------------------
%main functions
%------------------------------------------------------------------------
%select data folder
%------------------------------------------------------------------------
% --- Executes on button press in uiOpenFolder.
function uiOpenFolder_Callback(hObject, eventdata, handles)
% hObject    handle to uiOpenFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data_folder = uigetdir();
set(handles.uiFolder, 'String', data_folder)
acq_file = strcat(data_folder, '\acquistion.txt');
if exist(acq_file) ~= 0
    msgbox('The selected data folder already contains data. Upon acquisition this data will be overwritten!', 'LeXtender Acquisition', 'warn')
end

%------------------------------------------------------------------------
%test acquisition parameters
%------------------------------------------------------------------------
% --- Executes on button press in uiTest.
function uiTest_Callback(hObject, eventdata, handles)
% hObject    handle to uiTest (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%read UI values
data_folder = get(handles.uiFolder, 'String');
ul_position = str2num(get(handles.uiPosition, 'String'));
scan_resolution = str2num(get(handles.uiResolution, 'String'));
fields_hx = str2num(get(handles.uiField, 'String'));
fields_frequency = str2num(get(handles.uiFrequency, 'String'));
grab_max = str2num(get(handles.uiAveraging, 'String'));
wait_time = str2num(get(handles.uiHolding, 'String'));

%calculate LASER movement
move_laser = abs(ul_position) * 2 / scan_resolution;

%check data folder
if isempty(data_folder)
    msgbox('Please select Data Folder')
    return
end
hyst_file = strcat(data_folder, '\test.txt');
proc_file = strcat(data_folder, '\test_proc.txt');
if exist(hyst_file, 'file') ~= 0
    delete(hyst_file)
end
if exist(proc_file, 'file') ~= 0
    delete(proc_file)
end

%generate AutoMOKE commands
headerString = automoke_header(fields_hx, fields_frequency, grab_max);
positionString = automoke_movewait(move_laser, move_laser, wait_time);
measureString = automoke_measure;
exportString = automoke_export(data_folder, 'test');
homeString = automoke_gohome;
output = char(headerString, positionString, measureString, exportString, homeString);

%write AutoMOKE file and wait for execution
write_automoke('LeXtender_test.txt', output)
while check_automoke('LeXtender_test.txt') == 0
    pause(1)
end

%load test data
%hysteresis
hyst_array = import_hysteresis(data_folder, 'test');
axes(handles.axes1)
plot(hyst_array.data(:,1), hyst_array.data(:,2))
title('Test Acquisition Hysteresis')
xlabel(hyst_array.colheaders(1,1))
ylabel(hyst_array.colheaders(1,2))
%processed data
proc_array = import_processed(data_folder, 'test');
msg_hc = strcat(proc_array.textdata(1,1), '=', num2str(proc_array.data(1,1)));
msg_hoff = strcat(proc_array.textdata(2,1), '=', num2str(proc_array.data(2,1)));
msg_rem = strcat(proc_array.textdata(3,1), '=', num2str(proc_array.data(3,1)));
msg_amp = strcat(proc_array.textdata(4,1), '=', num2str(proc_array.data(4,1)));
msgbox([msg_hc msg_hoff msg_rem msg_amp], 'Test Processing Results')

%------------------------------------------------------------------------
%acquire data
%------------------------------------------------------------------------
% --- Executes on button press in uiAcquire.
function uiAcquire_Callback(hObject, eventdata, handles)
% hObject    handle to uiAcquire (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%read UI values
data_folder = get(handles.uiFolder, 'String');
ul_position = str2num(get(handles.uiPosition, 'String'));
scan_resolution = str2num(get(handles.uiResolution, 'String'));
fields_hx = str2num(get(handles.uiField, 'String'));
fields_frequency = str2num(get(handles.uiFrequency, 'String'));
grab_max = str2num(get(handles.uiAveraging, 'String'));
wait_time = str2num(get(handles.uiHolding, 'String'));
long_wait = '3';

%calculate LASER movement
move_laser = abs(ul_position) * 2 / scan_resolution;
limit_pos = abs(ul_position);
row_start = limit_pos;
row_end = 0 - limit_pos;
col_start = 0 - limit_pos;
col_end = limit_pos;

%initialize counter
data_counter = 1;
row_counter = row_start;
col_counter = col_start;
row_total = 2 * limit_pos / move_laser + 1;
col_total = 2 * limit_pos / move_laser + 1;
data_total = row_total * col_total + 1;

%generate reflectivity photo acquisition command
homeStr = automoke_gohome;
photoStr = automoke_photo;
pExportStr = automoke_photo_export(data_folder, 'photo');
output = char(homeStr, photoStr, pExportStr);

%write automoke and wait for execution
write_automoke('LeXtender_photo.txt', output)
while (check_automoke('LeXtender_photo.txt') == 0)
    pause(1)
end

%%%%%%%%%%%%%%%%%%%%%%
%measure line by line%
%%%%%%%%%%%%%%%%%%%%%%
while row_counter > (row_end - move_laser)
    %set measurement parameters and move to first point in line
    headerStr = automoke_header(fields_hx, fields_frequency, grab_max);
    lineStartStr = automoke_movewait(col_start, row_counter, long_wait);
    output = char(headerStr, lineStartStr);
    
    %measure line point by point
    col_counter = col_start;
    while col_counter < (col_end + move_laser)
        %determine datapoint
        datapoint_filename = strcat('y', num2str(row_counter), '_x', num2str(col_counter));
        data(data_counter, 1) = row_counter;
        data(data_counter, 2) = col_counter;
        
        %generate AutoMOKE commands
        positionStr = automoke_movewait(col_counter, row_counter, wait_time);
        measureStr = automoke_measure;
        exportStr = automoke_export(data_folder, datapoint_filename);
        output = char(output, positionStr, measureStr, exportStr);
        
        %increase point counter
        data_counter = data_counter + 1;
        col_counter = col_counter + move_laser;
    end
    
    %write AutoMOKE file
    automoke_filename = strcat('LeXtender_', num2str(data_counter), '_y', num2str(row_counter), '.txt');
    write_automoke(automoke_filename, output)
    while check_automoke(automoke_filename) == 0    
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %
        %        HIER sinnvollere time out aktion einf�gen
        %        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        pause(1)
    end
    
    %show last hysteresis of line
    hyst_array = import_hysteresis(data_folder, datapoint_filename);
    axes(handles.axes1)
    plot(hyst_array.data(:,1), hyst_array.data(:,2))
    title(strcat('Hysteresis ', int2str(data_counter), ' of ', int2str(data_total)))
    xlabel(hyst_array.colheaders(1,1))
    ylabel(hyst_array.colheaders(1,2))
    
    %increase line counter
    row_counter = row_counter - move_laser;
end

%write acquisition report
line1 = strcat('Position=', num2str(ul_position));
line2 = strcat('Resolution=', num2str(scan_resolution));
line3 = strcat('Field=', num2str(fields_hx));
line4 = strcat('Frequency=', num2str(fields_frequency));
line5 = strcat('Averaging=', num2str(grab_max));
line6 = strcat('Holding=', num2str(wait_time));
acq_header = char(line1, line2, line3, line4, line5, line6);
write_acquisition(data_folder, acq_header, data)

%data acquisition done
msgbox(strcat('Acquisition of ', int2str(data_counter), ' data points successful'), 'LeXtender Acquisition')


%------------------------------------------------------------------------
%reusable functions
%------------------------------------------------------------------------
%generate AutoMOKE commands
%------------------------------------------------------------------------
function returnString = automoke_header(fields_hx, fields_frequency, grab_max)
fields_hx_str = num2str(fields_hx);
fields_frequency_str = num2str(fields_frequency);
grab_max_str = num2str(grab_max);
line1 = 'laser_on()';
line2 = strcat('set_fields_hx(', fields_hx_str, ')');
line3 = strcat('set_fields_frequency(', fields_frequency_str, ')');
line4 = strcat('set_grab_maxaverages(', grab_max_str, ')');
line5 = strcat('set_process()');
returnString = char(line1, line2, line3, line4, line5);

function returnString = automoke_movewait(target_x, target_y, wait_time)
target_x_str = num2str(target_x);
target_y_str = num2str(target_y);
wait_time_str = num2str(wait_time);
line1 = strcat('set_target_x(', target_x_str, ')');
line2 = strcat('set_target_y(', target_y_str, ')');
line3 = strcat('go_to_target_position()');
line4 = strcat('wait(', wait_time_str, ')');
returnString = char(line1, line2, line3, line4);

function returnString = automoke_measure
line1 = 'start_grab()';
line2 = 'wait_grab_finished()';
returnString = char(line1, line2);

function returnString = automoke_export(data_folder, filename)
line1 = strcat('export_loop_text_NI(', data_folder, '\', filename, '.txt)');
line2 = strcat('export_processed_values_NI(', data_folder, '\', filename, '_proc.txt)');
returnString = char(line1, line2);

function returnString = automoke_gohome
line1 = 'go_home()';
line2 = 'wait(3)';
returnString = char(line1, line2);

function returnString = automoke_photo
line1 = 'start_photo()';
returnString = line1;

function returnString = automoke_photo_export(data_folder, filename)
line1 = strcat('save_image_NI(', data_folder, '\', filename, '.skm3)');
line2 = strcat('export_image_text_NI(', data_folder, '\', filename, '.txt)');
returnString = char(line1, line2);

%------------------------------------------------------------------------
%import data
%------------------------------------------------------------------------
function importArray = import_processed(data_folder, filename)
proc_file = strcat(data_folder, '\', filename, '_proc.txt');
delimiter = '=';
tempArray = importdata(proc_file, delimiter);
for i=1:4
    tempArray.textdata(i,1) = strtrim(tempArray.textdata(i,1));
end
importArray = tempArray;

function importArray = import_hysteresis(data_folder, filename)
hyst_file = strcat(data_folder, '\', filename, '.txt');
delimiter = '\t';
header = 1;
importArray = importdata(hyst_file, delimiter, header);

function finished = check_automoke(filename)
automoke_file = strcat('C:\NanoMOKEInbox\', filename);
if exist(automoke_file, 'file') == 0
    finished = 1;
else
    finished = 0;
end

%------------------------------------------------------------------------
%export data
%------------------------------------------------------------------------
function write_automoke(filename, output)
%automoke_file = strcat('C:\', filename)
automoke_file = filename;
inbox_file = strcat('C:\NanoMOKEInbox\', filename);
fileID = fopen(automoke_file, 'wt');
for i=1:size(output,1)
   fprintf(fileID, '%s\n', output(i,:));
end
fclose(fileID);
movefile(automoke_file, inbox_file)

function write_acquisition(data_folder, header, data)
acq_file = strcat(data_folder, '\acquisition.txt');
fileID = fopen(acq_file, 'wt');
for i=1:size(header,1)
   fprintf(fileID, '%s\n', header(i,:));
end
fclose(fileID);
dlmwrite(acq_file, data, '-append', 'newline', 'pc')
