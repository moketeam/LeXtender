%------------------------------------------------------------------------
%process forc data
%inputs: forc(filename or forc struct array), numInterpol, numSF, HbCutOff, wb_title
%------------------------------------------------------------------------
function output = forc_proc(varargin)
%start time
tic;

%take variable input
proc_config = struct();
switch nargin
    case 1
        proc_config.forc = varargin{1};
        proc_config.numInterpol = 61;
        proc_config.numSF = 3;
        proc_config.HbCutOff = 20;
        proc_config.title = 'LeXtender FORC Processing';
        proc_config.standAlone = 0;
    case 2
        proc_config.forc = varargin{1};
        proc_config.numInterpol = varargin{2};
        proc_config.numSF = 3;
        proc_config.HbCutOff = 20;
        proc_config.title = 'LeXtender FORC Processing';
        proc_config.standAlone = 0;
        
    case 3
        proc_config.forc = varargin{1};
        proc_config.numInterpol = varargin{2};
        proc_config.numSF = varargin{3};
        proc_config.HbCutOff = 20;
        proc_config.title = 'LeXtender FORC Processing';
        proc_config.standAlone = 0;
    case 4
        proc_config.forc = varargin{1};
        proc_config.numInterpol = varargin{2};
        proc_config.numSF = varargin{3};
        proc_config.HbCutOff = varargin{4};
        proc_config.title = 'LeXtender FORC Processing';
        proc_config.standAlone = 0;
    case 5
        proc_config.forc = varargin{1};
        proc_config.numInterpol = varargin{2};
        proc_config.numSF = varargin{3};
        proc_config.HbCutOff = varargin{4};
        proc_config.title = varargin{5};
        proc_config.standAlone = 0;
    otherwise
        [data_file, data_path] = uigetfile({'*.mat;*.forc','LeXtender FORC Acquisition File'});
        if data_file == 0
            return
        end
        proc_config.forc = fullfile(data_path, data_file);
        proc_config.numInterpol = 61;
        proc_config.numSF = 3;
        proc_config.HbCutOff = 20;
        proc_config.title = 'LeXtender Processing';
        proc_config.standAlone = 1;
end

%%load forc data and intialise settings
%load forc data
if ischar(proc_config.forc) == 1
    loadVariables = load(proc_config.forc, '-mat');
    forc = loadVariables.forc;
    forc_config = loadVariables.forc_config;
else
    forc = proc_config.forc;
end
%dataset size
numForc = size(forc,2);
numHb = size(forc(1).data,1);
%interpolation size
numInterpol = proc_config.numInterpol;
%smoothing factor
numSF = proc_config.numSF;
%minimum interpolation size
minInterpol = numInterpol - (2*numSF+1) - 1;
%Hb cutoff
HbCutOff = proc_config.HbCutOff;

%initialise parallelisation and warnings off
wb_title = proc_config.title;
wb = waitbar(0, 'Starting up parallelisation...', 'Name', wb_title);
myPool = gcp;
warning('off','all');
delete(wb)

%determine actual field magnitude in Hb
wb = cwaitbar(0, 'Analyzing actual Hb fields...', 'Name', wb_title);
parfor i=1:numForc
    minHb(i) = min(min(forc(i).data(HbCutOff:end,1)));
    maxHb(i) = max(max(forc(i).data(HbCutOff:end,1)));
end
minHb = min(minHb);
maxHb = max(maxHb);
HbI(1,:) = linspace(minHb, maxHb, numInterpol);

%resort and interpolate data in Hb
cwaitbar(0, wb, 'Interpolating Hb fields...', 'Name', wb_title);
MIa = NaN(numForc, numInterpol);
Ha = NaN(numForc, 1);
for j=1:numForc
    cwaitbar(j/numForc, wb);
    Ha(j,1) = min(min(forc(j).data(:,1)));
    curHb = forc(j).data(:,1);
    curM = forc(j).data(:,2);
    
    %%determine field values for interpolation; interpolate only for fields larger than Ha
    curHbI = NaN(1,numInterpol);
    % only interpolate where Hb > Ha
    curMinHbI = find(HbI(1,:) > Ha(j,1), 1, 'first');
    %always interpoalte some values
    if curMinHbI > minInterpol
        curMinHbI = minInterpol;
    end
    curHbI(curMinHbI:numInterpol) = HbI(1,curMinHbI:numInterpol);
    
    %%determine field values for extended FORC
    if curMinHbI < 3*numSF;
        curExHb = 1;
    else
        curExHb = curMinHbI - 3*numSF + 1;
    end

% original code before Soltan special VSM fix
%     %%interpolate data in Hb
%     try
%         MIa(j,:) = interp1(curHb, curM, curHbI);
%         MIa(j,curExHb:curMinHbI) = MIa(j,curMinHbI);
%     catch errDuplicateHb
%         %do stuff when interpolation fails due to double values of Hb
%         for l=1:numHb
%             for m=1:numHb-l
%                 if curHb(l) == curHb(m+l)
%                     %add 10 �Oe if two Hb values overlap
%                     curHb(m+l) = curHb(m+l) + 0.00001;
%                 end
%             end
%         end
%         MIa(j,:) = interp1(curHb, curM, curHbI);
%     end
    
    %%interpolate data in Hb
    try
        MIa(j,:) = interp1(curHb, curM, curHbI);
        MIa(j,curExHb:curMinHbI) = MIa(j,curMinHbI);
    catch errDuplicateHb
        %do stuff when interpolation fails due to double values of Hb
        for l=1:size(curHb,1)
            for m=1:size(curHb,1)-l
                if curHb(l) == curHb(m+l)
                    %add 10 �Oe if two Hb values overlap
                    curHb(m+l) = curHb(m+l) + 0.00001;
                end
            end
        end
        MIa(j,:) = interp1(curHb, curM, curHbI);
    end
end

%determine actual field magnitude in Ha
minHa = min(Ha);
maxHa = max(Ha);
HaI(:,1) = linspace(minHa, maxHa, numInterpol);

%resort and interpolate data in Ha
cwaitbar(0, wb, 'Interpolating Ha fields...', 'Name', wb_title);
MI = NaN(numInterpol, numInterpol,3);
MI(:,:,1) = repmat(HbI, numInterpol, 1);
MI(:,:,2) = repmat(HaI, 1, numInterpol);
for i=1:numInterpol
    warning('off','all');
    try
        MI(:,i,3) = interp1(Ha(:,1), MIa(:,i), HaI);
    catch errDublicateHa
        disp(errDublicateHa)
    end
end

%do fitting of surface approximation function to gain FORC density
fitpar(1:numInterpol,1:numInterpol) = struct('X', NaN, 'Y', NaN, 'M', NaN);
pMI = NaN(numInterpol, numInterpol);
cwaitbar(0, wb, 'Fitting FORC density...', 'Name', wb_title);
for i=1:numInterpol
    cwaitbar(i/numInterpol, wb);
    for j=1:numInterpol
        if ~isnan(MI(j,i,3))
            tempLength = 2*numSF+1;
            tempSize = tempLength^2;
            temp = NaN(tempLength,tempLength,3);
            try
                temp(:,:,:) = MI(j-numSF:j+numSF, i-numSF:i+numSF, :);
            catch errSort
                fitpar(j,i).X = NaN;
                fitpar(j,i).Y = NaN;
                fitpar(j,i).M = NaN;
            end
            tempX = reshape(temp(:,:,1), tempSize, 1);
            tempY = reshape(temp(:,:,2), tempSize, 1);
            tempM = reshape(temp(:,:,3), tempSize, 1);
            tempNaN = find(~isnan(tempM));
            if size(tempNaN,1) > 8
                fitpar(j,i).X = tempX(tempNaN);
                fitpar(j,i).Y = tempY(tempNaN);
                fitpar(j,i).M = tempM(tempNaN);
            else
                fitpar(j,i).X = NaN;
                fitpar(j,i).Y = NaN;
                fitpar(j,i).M = NaN;
            end
        end
    end
    parfor j=1:numInterpol
        warning('off','all');
        if ~isnan(fitpar(j,i).X)
            try
                pforc = fittype( @(a1,a2,a3,a4,a5,p,X,Y) a1 + a2*Y + a3*Y.^2 + a4*X + a5*X.^2 + p*X.*Y, 'indep', {'X', 'Y'}, 'dep', 'M');
                fitresult = fit([fitpar(j,i).X, fitpar(j,i).Y], fitpar(j,i).M, pforc);
                fitcoeffs = coeffvalues(fitresult);
                pMI(j,i) = fitcoeffs(6);
            catch errFit
                disp(errFit)
                %do something for fit
            end
        else
            pMI(j,i) = NaN;
        end
    end
end

%base transformation
cwaitbar(0, wb, 'Performing base transformation...', 'Name', wb_title);
parfor i=1:numInterpol
    Hu(:,i) = (HbI(1,i) + HaI(:,1))/2;
    Hc(:,i) = (HbI(1,i) - HaI(:,1))/2;
end
delete(wb)

%stop parallelisation
%wb = waitbar(0, 'Stopping parallelisation...', 'Name', wb_title);
%matlabpool close
%delete(wb);

%return output
output = struct;
try
    output.Hb = HbI;
    output.Ha = HaI;
    output.M = MI(:,:,3);
    output.Hc = Hc;
    output.Hu = Hu;
    output.pM = pMI;
    output.execTime = toc;
    if ischar(proc_config.forc) == 1
        output.config = forc_config;
    end
catch errOutput
    disp(errOutput)
end

%show data if in stand alone mode
if proc_config.standAlone == 1
    figure('Name', [proc_config.forc ' - magnetisation'])
    surf(output.Hb, output.Ha, output.M, 'EdgeColor', 'none')
    xlabel('Hb (Oe)')
    ylabel('Ha (Oe)')
    zlabel('M (mdeg)')
    figure('Name', [proc_config.forc ' - FORC density'])
    surf(output.Hc, output.Hu, output.pM, 'EdgeColor', 'none')
    xlabel('Hc (Oe)')
    ylabel('Hu (Oe)')
    zlabel('P')    
end